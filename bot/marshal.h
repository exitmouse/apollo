#ifndef __MARSHAL_HPP__
#define __MARSHAL_HPP__

#include <memory>
#include <boost/asio.hpp>
#include "../game/poker.h"
#include "../game/public_information_set.h"
#include "bot_interface.h"
using boost::asio::ip::tcp;

class Marshal {
public:
  Marshal(BotInterface *b) : bot_(b) {}
  void run(tcp::iostream &stream);
  static Card parseCardString(std::string cand);
  static std::vector<Action> parseActionString(std::string action, std::vector<Card> board);
private:
  static bool unusedToken(std::string token);
  static std::string printAction(PlayerAction a);
  // current game state
  std::shared_ptr<InformationSet> state_;
  BotInterface *bot_;
  std::vector<std::string> names_;
  int num_hands_ = 0;
};

#endif  // __MARSHAL_HPP__
