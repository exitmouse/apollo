#include <vector>
#include <iterator>
#include "game_abstracter.h"
#include "../montecarlo/montecarlo.h"
#include "../game/information_set.h"
#include "../game/action.h"
#include <typeinfo>

namespace abstract_game {

PocketBucket BucketCards(InformationSet info, int max_buckets) {
	MontecarloEvaluator eval = MontecarloEvaluator(
				info.hand, 
				info.public_state.board, 
				info.public_state.cards_seen, info.public_state.players_left());

	double wr = eval.RunIterationsByCount(100);
	
	return static_cast<PocketBucket>(floor(wr * max_buckets));
}


std::vector<AbstractPlayerAction> NextActionsPossible(const PublicInformationSet knowledge) {

  std::vector<PlayerAction> results{};
  // betting round over
  if (knowledge.BettingRoundDone()) { return results; }
  if (knowledge.IsValid(Card{0})) { return results; }

  // if it costs nothing to call
  if (knowledge.IsValid(Check())) {
    results.push_back(Check());
  } else {
    // we can call
    Call c{knowledge.call_amount_this_round()};
    assert(knowledge.IsValid(c));
    results.push_back(c);
    assert(knowledge.IsValid(Fold()));
    results.push_back(Fold());
  }
  std::vector<int> bins = { knowledge.minimum_raise(),
                            knowledge.fractional_pot_raise(2),
                            knowledge.maximum_raise() };
  for(int i = 0; i < 3; ++i) {
    int bet = bins[i];
    if(knowledge.IsValid(Bet(bet))) {
      results.push_back(Bet(i));
    } else if(knowledge.IsValid(Raise(bet))) {
      results.push_back(Raise(i));
    }
  }

  return results;
}

std::vector<AbstractAction> CondensedBetHistory(const PublicInformationSet knowledge) {
  std::vector<AbstractAction> result;
  for (auto it = begin(knowledge.total_summary_bet_amounts); it != end(knowledge.total_summary_bet_amounts); ++it) {
    result.push_back(SummaryBet{(*it)/SUMMARY_BET_BIN_WIDTH});
  }
  for (auto it = begin(knowledge.capped_abstractified_actions); it != end(knowledge.capped_abstractified_actions); ++it) {
    AbstractAction action = (*it);
    result.push_back(action);
  }	
  return result;
}
std::vector<AbstractAction> TruncatedBetHistory(const PublicInformationSet knowledge) {
  std::vector<AbstractAction> result;
  auto it = knowledge.capped_abstractified_actions.end();
  for (int i = 0; i < abstract_game::TRUNCATED_ABSTRACT_ACTION_MEMORY; ++i) {
    --it;
    if(it == knowledge.capped_abstractified_actions.begin()) {
      break;
    }
  }
  for (; it != knowledge.capped_abstractified_actions.end(); ++it) {
    AbstractAction action = *it;
    result.push_back(action);
  }
  return result;
}

std::vector<AbstractAction> CondensedBetHistoryNoMinBets(const PublicInformationSet knowledge) {
  std::vector<AbstractAction> result;
  for (auto it = begin(knowledge.total_summary_bet_amounts); it != end(knowledge.total_summary_bet_amounts); ++it) {
    result.push_back(SummaryBet{(*it)/SUMMARY_BET_BIN_WIDTH});
  }
  for (auto it = begin(knowledge.capped_abstractified_actions); it != end(knowledge.capped_abstractified_actions); ++it) {
    AbstractAction action = (*it);
    result.push_back(NoMinBetsOrRaises(action));
  }	
  return result;
}
std::vector<AbstractAction> TruncatedBetHistoryNoMinBets(const PublicInformationSet knowledge) {
  std::vector<AbstractAction> result;
  auto it = knowledge.capped_abstractified_actions.end();
  for (int i = 0; i < abstract_game::TRUNCATED_ABSTRACT_ACTION_MEMORY; ++i) {
    --it;
    if(it == knowledge.capped_abstractified_actions.begin()) {
      break;
    }
  }
  for (; it != knowledge.capped_abstractified_actions.end(); ++it) {
    AbstractAction action = *it;
    result.push_back(NoMinBetsOrRaises(action));
  }
  return result;
}


bool operator==(const AbstractInformationSet& lhs, const AbstractInformationSet& rhs) {
  return (lhs.phase == rhs.phase &&
          lhs.cards == rhs.cards &&
          lhs.current_player_role == rhs.current_player_role &&
          lhs.last_round_bet == rhs.last_round_bet &&
          lhs.has_folded == rhs.has_folded &&
          lhs.bets == rhs.bets);
}

};
