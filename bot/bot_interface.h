#ifndef BOT_INTERFACE_H_
#define BOT_INTERFACE_H_

#include <string>
#include <tuple>
#include <vector>
#include "../game/action.h"
#include "../game/information_set.h"

//The key value pairings we get for storage. TODO put in separate header
typedef std::vector<std::tuple<std::string, std::string> > KVList;

class BotInterface {
public:
  virtual ~BotInterface();
  virtual PlayerAction GetAction(const InformationSet&) = 0;
  virtual void InputKeys(const KVList) = 0;
  virtual KVList GetKeys(const InformationSet&) = 0;
};

#endif // BOT_INTERFACE_H_
