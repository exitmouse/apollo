#ifndef __GAME_ABSTRACTER_HPP__
#define __GAME_ABSTRACTER_HPP__

#include "../game/poker.h"
#include "../game/pocket_bucket.h"
#include "../game/information_set.h"
#include "../game/public_information_set.h"
#include "../game/action.h"

#include <vector>
#include <boost/functional/hash.hpp>

namespace abstract_game {

  const static int STARTING_STACK_SIZE = 10;
  const static int MAX_MIN_RAISES_IN_WALK = 0;
  const static int MAX_HALF_RAISES_IN_WALK = 0;
  const static int ABSTRACT_ACTION_MEMORY = 10;
  const static int TRUNCATED_ABSTRACT_ACTION_MEMORY = 10;
  const static int LAST_ROUND_BET_BIN_WIDTH = 3; //0 needs to be achievable
  const static int PLAYBACK_LAST_ROUND_BET_BIN_WIDTH = 60;
  const static int SUMMARY_BET_BIN_WIDTH = 2; // I am pretty sure
                                               // summary bets do not
                                               // help, but.


  PocketBucket BucketCards(InformationSet info, int max_buckets);
  inline PocketBucket BucketCards(InformationSet info) { return BucketCards(info, MAX_HAND_BUCKETS); }

// fold, check, call, bet, raise
  std::vector<AbstractPlayerAction> NextActionsPossible(const PublicInformationSet knowledge); 

  std::vector<AbstractAction> CondensedBetHistory(const PublicInformationSet knowledge);
  std::vector<AbstractAction> TruncatedBetHistory(const PublicInformationSet knowledge);
  std::vector<AbstractAction> CondensedBetHistoryNoMinBets(const PublicInformationSet knowledge);
  std::vector<AbstractAction> TruncatedBetHistoryNoMinBets(const PublicInformationSet knowledge);

  typedef std::vector<AbstractAction> AbstractBetHistory;

  typedef PocketBucket CardAbstraction;

  struct AbstractInformationSet {
    GamePhase phase;
    CardAbstraction cards;
    int current_player_role; // turn_index relative to button_index,
                             // ensures positional strategies are
                             // disjoint.
    int last_round_bet;
    std::array<bool, 3> has_folded; //NOTE: 0 index is button!!
    AbstractBetHistory bets; // Truncated even further for backup-strat
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* version */) {
      ar & phase;
      ar & cards;
      ar & current_player_role;
      ar & last_round_bet;
      ar & has_folded;
      ar & bets;
    }
  };
  bool operator==(const AbstractInformationSet& lhs, const AbstractInformationSet& rhs);
  
  /* only used in bot */
  inline AbstractInformationSet MakeAbstractInformationSet(Hand pocket, const PublicInformationSet* pinfo_ptr) {
    PublicInformationSet p = *pinfo_ptr;
    InformationSet info = InformationSet(p, pocket);
	
    PocketBucket pbucket = BucketCards(info);
    AbstractBetHistory bet_history = CondensedBetHistoryNoMinBets(p);

    AbstractInformationSet ret;
    ret.phase = p.phase;
    ret.cards = pbucket;
    ret.bets = bet_history;
    ret.current_player_role = p.current_player_role();
    ret.last_round_bet = p.last_round_bet / PLAYBACK_LAST_ROUND_BET_BIN_WIDTH;
    ret.has_folded = std::array<bool, 3>{p.has_folded[PublicInformationSet::Clockwise(p.button_index, 0)],
                                         p.has_folded[PublicInformationSet::Clockwise(p.button_index, 1)],
                                         p.has_folded[PublicInformationSet::Clockwise(p.button_index, 2)]};

    return ret;
  }


  struct aishash : std::unary_function<std::string, std::size_t>
  {
    std::size_t operator()(const AbstractInformationSet& as) const {
      std::size_t seed = static_cast<size_t>(as.cards);
      std::size_t phase = static_cast<size_t>(as.phase);
      std::size_t role = static_cast<size_t>(as.current_player_role);
      std::size_t lrb = static_cast<size_t>(as.last_round_bet);
      boost::hash_combine(seed, phase);
      boost::hash_combine(seed, role);
      boost::hash_combine(seed, lrb);
      boost::hash_combine(seed, boost::hash_value(as.has_folded[0]));
      boost::hash_combine(seed, boost::hash_value(as.has_folded[1]));
      boost::hash_combine(seed, boost::hash_value(as.has_folded[2]));

      std::vector<AbstractAction> history = as.bets;

      for(int i = 0; i < history.size(); ++i) {
        AbstractAction a = history[i];
        boost::hash_combine(seed, HashAbstractAction(a));
      }

      return seed;
    }
  };

};

#endif
