#include "bot_interface.h"
/* This .cc file is needed or the linker does not know where to put
 * the definition of this virtual (but not pure virtual) method, and includes
 * it in every implementer. Equivalently, it's needed to suppress a
 * weak-vtables error. */
BotInterface::~BotInterface() {}
