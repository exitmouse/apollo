#include <iostream>
#include "marshal.h"
#include <boost/algorithm/string.hpp>
#include "../game/poker.h"
#include "../game/value.h"

/**
 * Simple example pokerbot, written in C++.
 *
 * This is an example of a bare bones pokerbot. It only sets up the socket
 * necessary to connect with the engine (using a Boost iostream for
 * convenience) and then always returns the same action.  It is meant as an
 * example of how a pokerbot should communicate with the engine.
 */
void Marshal::run(tcp::iostream &stream) {
  std::string line;

  std::string getaction("GETACTION");
  std::string request_keyvalue_action("REQUESTKEYVALUES");
  std::string newhand("NEWHAND");
  std::string newgame("NEWGAME");
  std::string handover("HANDOVER");
  std::string action_check("CHECK");
  std::string action_call("CALL");


  while (std::getline(stream, line)) {
    std::cout << "Received line: " << line << "\n";

    // grab the first word
    std::string first_word = line.substr(0, line.find_first_of(' '));

    // split the line into tokens
    std::vector<std::string> tokens;
    boost::split(tokens, line, boost::is_any_of("\t "));

    using std::stoi;
    if (newgame.compare(first_word) == 0) {
      names_.clear();
      names_.push_back(tokens[1]);
      names_.push_back(tokens[2]);
      names_.push_back(tokens[3]);
      //int stack = stoi(tokens[4]);
      num_hands_ = stoi(tokens[6]);
    }

    // if we're starting a new hand, fill current table state...
    if (newhand.compare(first_word) == 0) {
      int hand_id = stoi(tokens[1]);
      int seat_num = stoi(tokens[2]);
      std::string my_name = names_[0];
      //given in seat 1, seat 2, seat 3 order
      std::vector<int> stack_sizes{stoi(tokens[5]), stoi(tokens[6]), stoi(tokens[7])};
      std::vector<int> info_stacks{stack_sizes};
      std::vector<std::string> name_at_position{tokens[8], tokens[9], tokens[10]};
      //0-indexed, unlike seat_num
      int position_num = 0;
      for (int i = 0; i < 3; ++i) {
        if (name_at_position[i] == my_name) {
          position_num = i;
        }
      }
      int button_index = (3 - position_num) % 3;
      //rotate so that we are in the position seat_num. This gives us name_at_seat
      std::rotate(begin(info_stacks), begin(info_stacks)+(seat_num-1), end(info_stacks));

      Card p1 = parseCardString(tokens[3]);
      Card p2 = parseCardString(tokens[4]);
      Hand pocket{p1, p2};
      /* TODO memory leak, but only when we get many newgame packets
       * in one initialization */
      state_ = std::make_shared<InformationSet>(hand_id, info_stacks, button_index, pocket);
    }


    // if we're being asked for an action, figure out wtf to do ...
    else if (getaction.compare(first_word) == 0) {
      //int pot_size = stoi(tokens[1]);
      int num_board_cards = stoi(tokens[2]);
      
      Hand board_state;
      /* used to construct the actions corresponding to a DEAL */
      std::vector<Card> board_in_order;
      // filling the board state
      for (int i = 0; i < num_board_cards; i++) {
        int ind = i + 3;
        Card card = parseCardString(tokens[ind]);
        board_state.AddCard(card);
        board_in_order.push_back(card);
      }
      //int active_players = stoi(tokens[3 + num_board_cards + 3]);
      int num_last_actions = stoi(tokens[3 + num_board_cards + 3 + 1 + 3]); // 15
      //int num_legal_actions = stoi(tokens[3 + num_board_cards + 3 + 1 + 3 + 1 + num_last_actions]); // 19
      for (int i = 0; i < num_last_actions; i++) {
        int ind = i + 11 + num_board_cards; // 16
        if (unusedToken(tokens[ind])) {
        } else {
          std::vector<Action> a = parseActionString(tokens[ind], board_in_order);
          state_->public_state.DoActions(a);
        }
      }
      PlayerAction bot_output = bot_->GetAction(*state_);
      std::string resp = printAction(bot_output);
      std::cout << "decided to " << resp << "..." << std::endl;
      stream << resp << std::endl;
    }
    // if we're being asked for keypairs, tell em we don't have none
    else if (request_keyvalue_action.compare(first_word) == 0) {
      // TODO print the bot's KVList.
      // FINISH indicates no more keyvalue pairs to store.
      stream << "FINISH\n";
    }
  }
  std::cout << "Gameover, engine disconnected.\n";
}

Card Marshal::parseCardString(std::string cand) {
  int8_t suit = -1;
  std::string s = cand.substr(1);
  if(s == "d") {
    suit = 1;
  } else if (s == "c") {
    suit = 0;
  } else if (s == "s") {
    suit = 3;
  }  else {
    suit = 2;
  }

  std::string r = cand.substr(0, 1) ;
  int8_t rank = -1;
  if (r == "2") {
    rank = 0;
  } else if (r == "3") {
    rank = 1;
  } else if (r == "4") {
    rank = 2;
  } else if (r == "5") {
    rank = 3;
  } else if (r == "6") {
    rank = 4;
  } else if (r == "7") {
    rank = 5;
  } else if (r == "8") {
    rank = 6;
  } else if (r == "9") {
    rank = 7;
  } else if (r == "T") {
    rank = 8;
  } else if (r == "J") {
    rank = 9;
  } else if (r == "Q") {
    rank = 10;
  } else if (r == "K") {
    rank = 11;
  } else {
    rank = 12;
  }

  return Card(rank, suit);
}

bool Marshal::unusedToken(std::string token) {
  std::vector<std::string> parts;
  boost::split(parts, token, boost::is_any_of(":"));
  std::string type = parts[0];
  if (type == "REFUND" || type == "SHOW" || type == "TIE" || type == "WIN") {
    /* we ignore refunds because we handle it ourselves when a player
     * folds. */
    return true;
  } else {
    return false;
  }
}

std::vector<Action> Marshal::parseActionString(std::string token, std::vector<Card> board) {
  std::vector<std::string> parts;
  boost::split(parts, token, boost::is_any_of(":"));
  std::string type = parts[0];
  using std::stoi;
  if (type == "BET") {
    Bet o(stoi(parts[1]));
    return std::vector<Action>{o};
  } else if (type == "POST") {
    Post o(stoi(parts[1]));
    return std::vector<Action>{o};
  } else if (type == "CALL") {
    Call o(stoi(parts[1]));
    return std::vector<Action>{o};
  } else if (type == "RAISE") {
    Raise o(stoi(parts[1]));
    return std::vector<Action>{o};
  } else if (type == "CHECK") {
    Check o;
    return std::vector<Action>{o};
  } else if (type == "FOLD") {
    return std::vector<Action>{Fold()};
  } else if (type == "DEAL") {
    std::string street = parts[1];
    if (street == "FLOP") {
      return std::vector<Action>{board[0], board[1], board[2]};
    } else if (street == "TURN") {
      return std::vector<Action>{board[3]};
    } else if (street == "RIVER") {
      return std::vector<Action>{board[4]};
    } else {
      assert(false);
    }
  } else {
    assert(false);
  }
}

class ActionPrinter : public boost::static_visitor<std::string> {
public:
  std::string operator()(Fold) const {
    return "FOLD";
  }
  std::string operator()(Check) const {
    return "CHECK";
  }
  std::string operator()(Call o) const {
    return std::to_string(o.amt).insert(0, "CALL:");
  }
  std::string operator()(Bet o) const {
    return std::to_string(o.amt).insert(0, "BET:");
  }
  std::string operator()(Post o) const {
    return std::to_string(o.amt).insert(0, "POST:");
  }
  std::string operator()(Raise o) const {
    return std::to_string(o.amt).insert(0, "RAISE:");
  }
};

std::string Marshal::printAction(PlayerAction a) {
  return boost::apply_visitor(ActionPrinter(), a);
}
