CXX = clang++ -std=c++11 -stdlib=libstdc++
WARNINGS = -Weverything -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-exit-time-destructors -Wno-padded -Wno-sign-conversion -Wno-sign-compare -Wno-missing-braces
CXXFLAGS = -O3 -I/usr/include/poker-eval/ $(WARNINGS)
LDLIBS = -lboost_system -lboost_program_options -lboost_serialization -lpthread -lboost_thread

TEST_SOURCES = $(wildcard *_test.cc)
SOURCES = $(filter-out $(TEST_SOURCES), $(wildcard */*.cc)) $(filter-out $(TEST_SOURCES), $(wildcard *.cc)) 
SRC_DIRS = bot game montecarlo
#PROGNAME = apollo
#BUILDDIR = bin
#DEBUGDIR = debug
TESTDIR = test

train: bin/apollo
	bin/apollo --train -t 8

debug: bin/debug/apollo
	bin/debug/apollo --train -t 1

bin/debug/apollo: *.h $(SOURCES)
	$(CXX) -g3 $(CXXFLAGS) $(SOURCES) $(LDLIBS) -o $@ 

assembly: *.h $(SOURCES)
	$(CXX) -c -emit-llvm -S $(CXXFLAGS) $(SOURCES) $(LDLIBS)

bin/apollo: *.h $(SOURCES)
	$(CXX) $(CXXFLAGS) $(WARNINGS) $(SOURCES) $(LDLIBS) -o $@

bin/apollo2: *.h $(SOURCES)
	$(CXX) $(CXXFLAGS) $(WARNINGS) $(SOURCES) $(LDLIBS) -o $@

bin/apollo3: *.h $(SOURCES)
	$(CXX) $(CXXFLAGS) $(WARNINGS) $(SOURCES) $(LDLIBS) -o $@

perf.data: bin/debug/apollo
	perf record -b bin/debug/apollo

apollo.prof: bin/debug/apollo perf.data
	create_llvm_prof --binary=bin/debug/apollo --out=apollo.prof

bin/profiled/apollo: apollo.prof $(SOURCES) *.h
	$(CXX) $(CXXFLAGS) $(SOURCES) -fprofile-sample-use=apollo.prof -o $@

valgrind: bin/debug/apollo
	valgrind --vgdb-error=1 --track-origins=yes bin/debug/apollo --train -t 1

callgrind.out: bin/apollo
	valgrind --tool=callgrind --callgrind-out-file=$@ $< --train

callgrind_annotate.out: callgrind.out
	callgrind_annotate --inclusive=yes --tree=both --auto=yes $< > callgrind_annotate.out

cachegrind.out: bin/apollo
	valgrind --tool=cachegrind --cachegrind-out-file=$@ $< --train

cachegrind_annotate.out: cachegrind.out
	cachegrind_annotate --inclusive=yes --tree=both --auto=yes $< > cachegrind_annotate.out

# Points to the root of Google Test, relative to where this file is.
# Remember to tweak this if you move this file.
GTEST_DIR = $(TESTDIR)/gtest-1.7.0

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

CPP_FLAGS += -isystem $(GTEST_DIR)/include
CXX_FLAGS += -pthread

# Usually you shouldn't tweak such internal variables, indicated by a
# trailing _.
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

# For simplicity and to avoid depending on Google Test's
# implementation details, the dependencies specified below are
# conservative and not optimized.  This is fine as Google Test
# compiles fast and for ordinary users its source rarely changes.
gtest-all.o: $(GTEST_SRCS_)
	$(CXX) $(CPP_FLAGS) -I$(GTEST_DIR) $(CXX_FLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc

gtest_main.o: $(GTEST_SRCS_)
	$(CXX) $(CPP_FLAGS) -I$(GTEST_DIR) $(CXX_FLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc

gtest.a: gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

gtest_main.a: gtest-all.o gtest_main.o
	$(AR) $(ARFLAGS) $@ $^

%_test.o: %_test.cc
	$(CXX) $(CPP_FLAGS) -I$(GTEST_DIR)/include $(CXX_FLAGS) -c -o $@ $<

$(TESTDIR)/%_test: %_test.o $(patsubst %.cc, %.o, $(filter-out main.cc, $(SOURCES))) gtest_main.a
	$(CXX) $(CPP_FLAGS) $(CXX_FLAGS) $^ $(LDLIBS) -o $@

%_test: $(TESTDIR)/%_test
	$^ --gtest_color=yes


tests: $(filter-out timer_test, $(patsubst %.cc, %, $(TEST_SOURCES)))

clean:
	rm *.o *.a *.ll bin/debug/* test/*_test bot/*.o montecarlo/*.o game/*.o

apollo.zip:
	zip apollo.zip *
