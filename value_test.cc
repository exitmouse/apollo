#include <gtest/gtest.h>
#include "game/value.h"
#include "game/poker.h"
#include "game/printer.h"

namespace poker_value {

::testing::AssertionResult IsValueGT(uint64_t large_val, uint64_t small_val) {
  if (large_val > small_val) {
    return ::testing::AssertionSuccess();
  } else {
    return ::testing::AssertionFailure()  << \
      NotationValue(large_val) << " vs. " << \
      NotationValue(small_val);
  }
}

class ValueTest : public ::testing::Test {
public:
  Hand royal_flush; 
  Hand straight_flush; 
  Hand four_of_a_kind_high;
  Hand four_of_a_kind_low;
  Hand full_house_high;
  Hand full_house_low;
  Hand flush_high;
  Hand flush_low;
  Hand flush_low_last_two;
  Hand straight_high;
  Hand straight_low;
  Hand wheel;
  Hand three_of_a_kind_high;
  Hand three_of_a_kind_low;
  Hand two_pair_high;
  Hand two_pair_low;
  Hand pair_high;
  Hand pair_low;
  Hand high;
  Hand low;
  Hand zero;
  Hand pocket;
  std::vector<Hand> hands;
  virtual void SetUp() {
    //Except for four of a kind, we make sure not to use spades (for
    //the suit invariance test)
    royal_flush.set_cards(0x1f00); //0b1111100000000
    straight_flush.set_cards(0x01f0); //0b0000111110000
    four_of_a_kind_high.AddCard(Card(10,0));
    four_of_a_kind_high.AddCard(Card(10,1));
    four_of_a_kind_high.AddCard(Card(10,2));
    four_of_a_kind_high.AddCard(Card(10,3));
    four_of_a_kind_high.AddCard(Card(1,2));
    four_of_a_kind_low.AddCard(Card(9,0));
    four_of_a_kind_low.AddCard(Card(9,1));
    four_of_a_kind_low.AddCard(Card(9,2));
    four_of_a_kind_low.AddCard(Card(9,3));
    four_of_a_kind_low.AddCard(Card(7,3));
    full_house_high.AddCard(Card(10,0));
    full_house_high.AddCard(Card(10,1));
    full_house_high.AddCard(Card(10,2));
    full_house_high.AddCard(Card(1,2));
    full_house_high.AddCard(Card(1,1));
    full_house_low.AddCard(Card(9,0));
    full_house_low.AddCard(Card(9,1));
    full_house_low.AddCard(Card(9,2));
    full_house_low.AddCard(Card(8,0));
    full_house_low.AddCard(Card(8,2));
    flush_high.AddCard(Card(12,0));
    flush_high.AddCard(Card(4,0));
    flush_high.AddCard(Card(0,0));
    flush_high.AddCard(Card(1,0));
    flush_high.AddCard(Card(2,0));
    flush_low.AddCard(Card(11,0));
    flush_low.AddCard(Card(9,0));
    flush_low.AddCard(Card(8,0));
    flush_low.AddCard(Card(7,0));
    flush_low.AddCard(Card(6,0));
    flush_low_last_two.AddCard(Card(11,0));
    flush_low_last_two.AddCard(Card(9,0));
    flush_low_last_two.AddCard(Card(8,0));
    flush_low_last_two.AddCard(Card(6,0));
    flush_low_last_two.AddCard(Card(5,0));
    straight_high.AddCard(Card(12,0));
    straight_high.AddCard(Card(11,1));
    straight_high.AddCard(Card(10,0));
    straight_high.AddCard(Card(9,0));
    straight_high.AddCard(Card(8,0));
    straight_low.AddCard(Card(6,0));
    straight_low.AddCard(Card(5,0));
    straight_low.AddCard(Card(4,1));
    straight_low.AddCard(Card(3,0));
    straight_low.AddCard(Card(2,0));
    wheel.AddCard(Card(3,0));
    wheel.AddCard(Card(2,0));
    wheel.AddCard(Card(1,1));
    wheel.AddCard(Card(0,0));
    wheel.AddCard(Card(12,0));
    three_of_a_kind_high.AddCard(Card(10,0));
    three_of_a_kind_high.AddCard(Card(10,1));
    three_of_a_kind_high.AddCard(Card(10,2));
    three_of_a_kind_high.AddCard(Card(1,2));
    three_of_a_kind_high.AddCard(Card(8,0));
    three_of_a_kind_low.AddCard(Card(9,0));
    three_of_a_kind_low.AddCard(Card(9,1));
    three_of_a_kind_low.AddCard(Card(9,2));
    three_of_a_kind_low.AddCard(Card(6,2));
    three_of_a_kind_low.AddCard(Card(7,0));
    two_pair_high.AddCard(Card(10,0));
    two_pair_high.AddCard(Card(10,1));
    two_pair_high.AddCard(Card(1,2));
    two_pair_high.AddCard(Card(1,0));
    two_pair_high.AddCard(Card(8,1));
    two_pair_low.AddCard(Card(9,0));
    two_pair_low.AddCard(Card(9,1));
    two_pair_low.AddCard(Card(7,2));
    two_pair_low.AddCard(Card(7,0));
    two_pair_low.AddCard(Card(6,2));
    pair_high.AddCard(Card(10,0));
    pair_high.AddCard(Card(10,1));
    pair_high.AddCard(Card(1,2));
    pair_high.AddCard(Card(2,0));
    pair_high.AddCard(Card(8,0));
    pair_low.AddCard(Card(9,0));
    pair_low.AddCard(Card(9,1));
    pair_low.AddCard(Card(8,0));
    pair_low.AddCard(Card(7,2));
    pair_low.AddCard(Card(6,2));
    high.AddCard(Card(10,0));
    high.AddCard(Card(9,1));
    high.AddCard(Card(1,2));
    high.AddCard(Card(2,0));
    high.AddCard(Card(8,0));
    low.AddCard(Card(1,0));
    low.AddCard(Card(9,1));
    low.AddCard(Card(8,0));
    low.AddCard(Card(7,2));
    low.AddCard(Card(6,2));
    pocket.AddCard(Card(3,1));
    pocket.AddCard(Card(11,2));
    hands = { royal_flush 
            , straight_flush 
            , four_of_a_kind_high
            , four_of_a_kind_low
            , full_house_high
            , full_house_low
            , flush_high
            , flush_low
            , flush_low_last_two
            , straight_high
            , straight_low
            , wheel
            , three_of_a_kind_high
            , three_of_a_kind_low
            , two_pair_high
            , two_pair_low
            , pair_high
            , pair_low
            , high
            , low};
  }
  virtual void TearDown() {}
}; // class ValueTest

TEST_F(ValueTest, RanksHandsInOrder) {
  for (int i = 0; i < hands.size()-1; ++i) {
    EXPECT_GT(hands[i].Value(), hands[i+1].Value()) << \
      "Fails at index " << i                        << \
      " where "         << NotationHand(hands[i])   << \
      " is worse than " << NotationHand(hands[i+1]);
  }
}

//Idempotence
TEST_F(ValueTest, ReturnsSameResult) {
  for (int i = 0; i < hands.size(); ++i) {
    EXPECT_EQ(hands[i].Value(), hands[i].Value())   << \
      "Fails at index " << i                        << \
      " where hand is " << NotationHand(hands[i]);
  }
}

TEST_F(ValueTest, RanksHandsInOrderWithPocket) {
  for (int i = 0; i < hands.size()-1; ++i) {
    EXPECT_TRUE(IsValueGT(hands[i].ValueWithSeven(pocket), hands[i+1].ValueWithSeven(pocket))) << \
      "Fails at index " << i                        << \
      " where "         << NotationHand(hands[i])   << \
      " is worse than " << NotationHand(hands[i+1]) << \
      " with pocket " << NotationHand(pocket);
  }
}

//Idempotence
TEST_F(ValueTest, ReturnsSameResultWithPocket) {
  for (int i = 0; i < hands.size(); ++i) {
    EXPECT_EQ(hands[i].ValueWithSeven(pocket), hands[i].ValueWithSeven(pocket))   << \
      "Fails at index " << i                        << \
      " where hand is " << NotationHand(hands[i])   << \
      " with pocket " << NotationHand(pocket);
  }
}

TEST_F(ValueTest, TiesNoPairs) {
  Hand no_pair1, no_pair2;
  no_pair1.AddCard(Card(1,0));
  no_pair1.AddCard(Card(2,0));
  no_pair1.AddCard(Card(8,0));
  no_pair1.AddCard(Card(10,0));
  no_pair1.AddCard(Card(11,1));
  no_pair2.AddCard(Card(1,2));
  no_pair2.AddCard(Card(2,2));
  no_pair2.AddCard(Card(8,2));
  no_pair2.AddCard(Card(10,2));
  no_pair2.AddCard(Card(11,3));
  EXPECT_EQ(no_pair1.Value(), no_pair2.Value());
}

TEST_F(ValueTest, TiesSuitShifts) {
  for (int i = 0; i < hands.size(); ++i) {
    uint64_t cards = hands[i].cards();
    if (cards < (1ULL << 39)) { //Don't test hands with spades
      Hand shifted(cards << 13);
      EXPECT_EQ(hands[i].Value(), shifted.Value());
    }
  }
}

class HandTableTest : public ::testing::Test {
public:
  uint16_t *pop;
  uint16_t *straight;
  uint16_t *top_part;
  uint16_t *flush_tiebreaker;
  virtual void SetUp() {
    pop = GetHandTables()->pop_count;
    straight = GetHandTables()->straight_index;
    top_part = GetHandTables()->top_part;
    flush_tiebreaker = GetHandTables()->flush_tiebreaker;
  }
  virtual void TearDown() {}
}; // class HandTableTest

TEST_F(HandTableTest, correctPopCounts) {
  EXPECT_EQ(pop[0x1f],5);
  EXPECT_EQ(pop[0x10f],5);
  EXPECT_EQ(pop[0xf],4);
  EXPECT_EQ(pop[0x7],3);
  EXPECT_EQ(pop[0x3],2);
  EXPECT_EQ(pop[0x1],1);
  EXPECT_EQ(pop[0x0],0);
}

TEST_F(HandTableTest, correctStraightValues) {
  EXPECT_EQ(straight[0x1f], 4);
  EXPECT_EQ(straight[(1<<12) | 0x1f], 4);
  EXPECT_EQ(straight[(1<<12) | 0xf], 3);
  EXPECT_EQ(straight[(1<<12) | 0xf0], 0);
}

TEST_F(HandTableTest, correctTopPartValues) {
  EXPECT_EQ(top_part[0x1f], 0x1c);
  EXPECT_EQ(top_part[(1<<12) | 0x1f], (1<<12) | 0x1c);
  EXPECT_EQ(top_part[0xf0], 0xc0);
  EXPECT_EQ(top_part[0b0101110101001], 0b0101110100000);
  EXPECT_EQ(top_part[0b0101110000000], 0b0101000000000);
}

TEST_F(HandTableTest, correctFlushTiebreakerValues) {
  EXPECT_EQ(flush_tiebreaker[0x1f], 0x1f);
  EXPECT_EQ(flush_tiebreaker[(1<<12) | 0x1f], (1<<12) | 0x1e);
  EXPECT_EQ(flush_tiebreaker[0xf0], 0xf0);
  EXPECT_EQ(flush_tiebreaker[0b0101110101001], 0b0101110100000);
  EXPECT_EQ(flush_tiebreaker[0b0101110000000], 0b0101110000000);
}

} // namespace poker_value
