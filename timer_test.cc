#include <gtest/gtest.h>
#include <poker-eval/inlines/eval.h>
#include "timer.h"
#include "game/poker.h"
#include "game/value.h"

namespace timer {

template <typename TimeT = sc::milliseconds>
::testing::AssertionResult TimeUnder(TimeT small, TimeT large) {
  sc::milliseconds::rep small_c = sc::duration_cast<sc::milliseconds>(small).count();
  sc::milliseconds::rep large_c = sc::duration_cast<sc::milliseconds>(large).count();
  if (small_c < large_c) {
    return ::testing::AssertionSuccess() << \
      "Took " << small_c << " ms vs. " << large_c << " ms";
  } else {
    return ::testing::AssertionFailure()  << \
      "Took " << small_c << " ms vs. " << large_c << " ms";
  }
}

class TimingTest : public ::testing::Test {
public:
  poker_value::EvalTables *eval_tables;
  virtual void SetUp() {
    eval_tables = poker_value::GetEvalTables();
  }
};

uint64_t EvaluateHandsValue(int);
uint64_t EvaluateHandsPokerEval(int);
uint64_t EvaluateHandsEval7(int);

uint64_t EvaluateHandsValue(int num) {
  int i = 0;
  uint64_t x = 0;
  Hand zero;
  for(Hand h = Hand::First(7); i < num; ++i) {
    uint64_t cards = h.cards();
    uint64_t y = poker_value::ValueSevenCardHand(cards);
    if (y > x) { x = y; }
    if (!h.NextHand(52)) {
      break;
    }
  }
  return x;
}

uint64_t EvaluateHandsPokerEval(int num) {
  int i = 0;
  uint64_t x = 0;
  Hand zero;
  for(Hand h = Hand::First(7); i < num; ++i) {
    uint64_t cards = h.cards();
    uint64_t y = StdDeck_StdRules_EVAL_N(*reinterpret_cast<StdDeck_CardMask*>(&cards), 7);
    if (y > x) { x = y; }
    if (!h.NextHand(52)) {
      break;
    }
  }
  return x;
}

uint64_t EvaluateHandsEval7(int num) {
  int i = 0;
  uint64_t x = 0;
  Hand zero;
  for(Hand h = Hand::First(7); i < num; ++i) {
    uint64_t cards = h.cards();
    uint64_t y = poker_value::Eval7(cards);
    if (y > x) { x = y; }
    if (!h.NextHand(52)) {
      break;
    }
  }
  return x;
}

TEST_F(TimingTest, Eval7FasterThanPokerEval) {
  EXPECT_TRUE(
    TimeUnder(timer::Duration<>(EvaluateHandsEval7, 150000000),
              timer::Duration<>(EvaluateHandsPokerEval, 150000000))
  );
}

}
