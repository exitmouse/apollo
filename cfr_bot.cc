#include "cfr_bot.h"
#include "rng.h"
#include "bot/game_abstracter.h"
#include "strategy.h"
#include "montecarlo/montecarlo.h"

PlayerAction CFRBot::GetAction(const InformationSet& s) {
  const PublicInformationSet &state = s.public_state;

  // create the abstract info set for the current game state
  abstract_game::AbstractInformationSet infoset = abstract_game::MakeAbstractInformationSet(s.hand, &state);

  std::vector<AbstractPlayerAction> possible_actions = abstract_game::NextActionsPossible(state);
  assert(possible_actions.size() > 0);
  ActionDistribution dist;
  if (strategy.InBounds(infoset)) {
    // try and grab the appropriate strategy
    dist = strategy[infoset];
  } else {
    abstract_game::AbstractInformationSet short_infoset = infoset;
    short_infoset.bets = abstract_game::TruncatedBetHistoryNoMinBets(state);
    short_infoset.last_round_bet = 0;
    dist = short_strategy[short_infoset];
    std::cout << "Resorting to short strategy." << std::endl;
  }
  double rn = std::uniform_real_distribution<>(0,1)(GetMersenneTwister());
  AbstractPlayerAction a = Check{}; //can't default-initialize
  for(int8_t i = 0; i < possible_actions.size(); i++) {
    if(!dist.InBounds(i)) {
      a = possible_actions[i-1];
    } else {
      double prob = dist.ProbabilityOf(i);
      PlayerAction action = possible_actions[i];
      if(rn < prob) {
        a = action;
      } else {
        rn -= prob;
      }
    }
  }
  /* Translates with respect to the real game, not the abstract game TODO*/
  return boost::apply_visitor<>(PlayerActionTranslator{&state},a);
}
