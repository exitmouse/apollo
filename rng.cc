#include "rng.h"

namespace {
  thread_local std::random_device *rd;
  thread_local std::mt19937 *mersenne_twister;
  thread_local bool rng_init = false;
} // namespace

std::mt19937 &GetMersenneTwister() {
  if (!rng_init) {
    rd = new std::random_device{};
    mersenne_twister = new std::mt19937{(*rd)()};
  }
  return *mersenne_twister;
}
