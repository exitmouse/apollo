#ifndef CFR_PLUS_H_
#define CFR_PLUS_H_

#include <vector>
#include <forward_list>
#include "game/public_information_set.h"
#include "strategy.h"

namespace cfr {


/* Convention: the indices in these vectors go in the order of
 * NextHand(); that is, index i corresponds to Hand h => index i+1
 * corresponds to Hand h.NextHand() */
typedef std::array<double, 1326> prob_by_pocket_t;
/* the output of regret matching */
typedef std::array<ActionDistribution, 1326> dist_by_pocket_t;
/* Expected utility of a terminal node times the probability of
 * reaching it, given a specific pocket index */
typedef std::array<double, 1326> utility_from_pocket_t;

/* Must be called before multi-threaded use */
void Initialize();

utility_from_pocket_t Product(prob_by_pocket_t a, utility_from_pocket_t b);
/* Indicator function for pocket & cards == 0 */
prob_by_pocket_t PossibleHands(uint64_t cards);

/* This class keeps track of our probability of reaching the current
 * state indexed by first player number and then pocket index. The
 * top frame is all we care about for the current state. */
class PBPStack {
public:
  PBPStack() {
    for (int i = 0; i < 3; ++i) {
      stacks_[i].push_front(PossibleHands(0));
    }
  }
  /* push onto all three stacks the updated probabilities if a
   * specific card is dealt */
  void push_card(Card c) {
    prob_by_pocket_t frame = PossibleHands(Hand{c}.cards());
    for (int i = 0; i < 3; ++i) {
      push_frame(frame, i);
    }
  }
  /* pop all */
  void pop() {
    for (int i = 0; i < 3; ++i) {
      stacks_[i].pop_front();
    }
  }
  void push_frame(prob_by_pocket_t s, int player_num) {
    assert(player_num >= 0);
    assert(player_num < 3);
    assert(!stacks_[player_num].empty());
    stacks_[player_num].push_front(Product(stacks_[player_num].front(), s));
  }
  /* pop a player's stack */
  void pop(int player_num) {
    stacks_[player_num].pop_front();
  }
  double get(int player_num, int hand_index) {
    assert(player_num >= 0);
    assert(player_num < 3);
    assert(hand_index >= 0);
    if (hand_index >= 1326) {
      printf("%d\n", hand_index);
    }
    assert(hand_index < 1326);
    double ret = stacks_[player_num].front()[hand_index];
    return ret;
  }
private:
  /* ok to default initialize because we push to the empty forward
   * lists which default initialization creates.*/
  std::array<std::forward_list<prob_by_pocket_t>, 3> stacks_;
};


class CFRPlusTrainer {
public:
  /* Each abstractinfoset specifies its player, so we can store the
   * entire profile in one object. */
  CFRPlusTrainer(Strategy *s, Regret *r, Strategy *ss, Regret *sr) : average_strategy_profile(s),
                                                                     regret_profile(r),
                                                                     short_average_strategy_profile(ss),
                                                                     short_regret_profile(sr) {}
  Strategy *average_strategy_profile;
  Regret *regret_profile;
  Strategy *short_average_strategy_profile;
  Regret *short_regret_profile;
  void Iterate();
  utility_from_pocket_t PublicChanceSampleWalk(const PublicInformationSet *p, int player_num);
  prob_by_pocket_t RegretMatch(const PublicInformationSet *p, int8_t a, int choices);
  int walk_count = 0;
  int depth = 0;
private:
  std::array<double,1326> ComponentsAdd(std::array<double,1326> lhs, std::array<double,1326> rhs) {
    std::array<double,1326> ret{};
    for (int i = 0; i < 1326; ++i) {
      ret[i] = lhs[i] + rhs[i];
    }
    return ret;
  }
  /* default initialized to p(get here | pocket) = 1 */
  PBPStack probs_by_pocket_stack{};
  abstract_game::PocketBucketStack buckets_by_pocket_stack{};
  /* win prob, loss prob, sum is 1-tie prob */
  std::array<prob_by_pocket_t,3> PocketEquities(int opp);
  std::array<prob_by_pocket_t,5> PocketEquitiesVsTwoOpponents(int opp1, int opp2);
  bool val_sort_init = false;
  std::array<std::pair<uint64_t, int>, 1326> *val_sort;
  void InitValSort(const PublicInformationSet *p);
  std::array<std::pair<uint64_t, int>, 1326> *GetValSort();
  void PopValSort();
  /* for terminal states. We only call this in
   * PublicChanceSampleWalk, so we assume the state is terminal and
   * that probs_by_pocket_stack has been correctly dealt with. */
  utility_from_pocket_t CalculateUtilities(const PublicInformationSet *p, int player_num);
}; // class CFRPlusTrainer

} // namespace cfr

#endif // CFR_PLUS_H_
