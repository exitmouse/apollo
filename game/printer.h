/* printer.h:
 * Prints hands and cards in familiar notation like AH, TD, 3S. */
#ifndef PRINTER_H_
#define PRINTER_H_

#include <string>
#include "poker.h"

std::string NotationCard(Card const c);
std::string NotationHand(Hand const h);
std::string NotationValue(uint64_t const v);

#endif //PRINTER_H_
