/* action.h
 * This file defines the potential actions in a hand of poker.
 * An ActionSupplier outputs actions in response to getactions.
 */
#ifndef ACTION_H_
#define ACTION_H_

#include "boost/array.hpp"
#include "boost/serialization/array.hpp"
#include "boost/variant.hpp"
#include "card.h"
#include <boost/serialization/serialization.hpp>
#include <iostream>
/* Fold */
struct Fold {
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & /* ar */, const unsigned int /* version */) {
  }
};

/* Check */
struct Check {
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & /* ar */, const unsigned int /* version */) {
  }
};

/* Call (includes amount, in case of all-in)*/
struct Call {
  Call(): amt(0) {}
  Call(int i) : amt(i) {}
  int amt;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & amt;
  }
};

/* Bet (amt is your total contribution to the pool this betting
 * round) */
struct Bet {
  Bet(): amt(0) {}
  Bet(int i) : amt(i) {}
  int amt;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & amt;
  }
};

/* Raise (amt is your total contribution to the pool this betting
 * round) */
struct Raise {
  Raise(): amt(0) {}
  Raise(int i) : amt(i) {}
  int amt;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & amt;
  }
};

/* Post (posting of blinds); handled exactly like Bet but printed
 * differently. */
struct Post {
  Post(): amt(0) {}
  Post(int i) : amt(i) {}
  int amt;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & amt;
  }
};

/* SummaryBet is an aggregation of a player's previous bets in a single betting round
   if there's been too much rereaising */
struct SummaryBet {
  SummaryBet(): amt(0) {}
  SummaryBet(int i) : amt(i) {}
  int amt;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & amt;
  }
};

bool operator==(const Fold&, const Fold&);
bool operator==(const Check&, const Check&);
bool operator==(const Call&, const Call&);
bool operator==(const Bet&, const Bet&);
bool operator==(const Raise&, const Raise&);
bool operator==(const Post&, const Post&);
bool operator==(const SummaryBet&, const SummaryBet&);

/* does not include Post because our bots never have to specify that
 * they post the blinds. */
typedef boost::variant<Fold, Check, Call, Bet, Raise> PlayerAction;
typedef boost::variant<Fold, Check, Call, Bet, Raise, Post, Card> Action;
typedef boost::variant<Fold, Check, Call, Bet, Raise> AbstractPlayerAction;
typedef boost::variant<Fold, Check, Call, Bet, Raise, Post, Card, SummaryBet> AbstractAction;


Action Absolutify(Action a, int i);
AbstractAction NoMinBetsOrRaises(AbstractAction a);
int ActionAmount(AbstractAction a);
std::string StringAbstractAction(AbstractAction a);

struct WAbstractAction {
  AbstractAction action_;

  WAbstractAction(AbstractAction a) : action_(a) {}
  
  AbstractAction action() {
    return action_;
  }
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & action_;
  }
};

typedef int16_t hash_t;

hash_t HashAction(Action a);
hash_t HashAbstractAction(AbstractAction a);

#endif //ACTION_H_
