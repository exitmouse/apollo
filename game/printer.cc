#include "printer.h"
#include <iostream>
#include <sstream>
#include <string>

std::string NotationCard(Card const c){
  char suit = c.Suit();
  char rank = c.Rank();
  char suit_sym, rank_sym;
  switch(suit){
    case 0: {
      suit_sym = 'C';
      break;
    }
    case 1: {
      suit_sym = 'D';
      break;
    }
    case 2: {
      suit_sym = 'H';
      break;
    }
    case 3: {
      suit_sym = 'S';
      break;
    }
    default: {
      assert(false);
    }
  }
  switch(rank){
    case 12: {
      rank_sym = 'A';
      break;
    }
    case 11: {
      rank_sym = 'K';
      break;
    }
    case 10: {
      rank_sym = 'Q';
      break;
    }
    case 9: {
      rank_sym = 'J';
      break;
    }
    case 8: {
      rank_sym = 'T';
      break;
    }
    default: {
      rank_sym = '2' + rank;
      break;
    }
  }
  std::string s;
  s.append(1, rank_sym);
  s.append(1, suit_sym);
  return s;
}

std::string NotationHand(Hand const h) {
  std::string s;
  if(h.cards() == 0) {
    return s;
  }
  for (char i = 51; i >= 0; --i) { //Highest first
    Card c = static_cast<Card>(i);
    if (h.Contains(c)) {
      s.append(NotationCard(c));
      s.push_back(' ');
    }
  }
  s.pop_back(); //get rid of trailing space
  return s;
}

std::string NotationValue(uint64_t const v) {
  std::stringstream s;
  s << "(" << (v>>26) << ", ";
  s << std::hex << (((1 << 13)-1) & (v >> 13)) << ", ";
  s << std::hex << (((1 << 13) - 1) & v) << ")";
  return s.str();
}
