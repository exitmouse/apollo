#ifndef INFORMATION_SET_H_
#define INFORMATION_SET_H_

#include "public_information_set.h"

struct InformationSet {
  Hand hand;
  PublicInformationSet public_state;
  InformationSet(int n,
                 std::vector<int> ss,
                 int b,
                 Hand h)
    : hand(h),
      public_state(n, ss, b) {}

  InformationSet(PublicInformationSet p, Hand h)
  : hand(h), public_state(p) {}
  
};

#endif // INFORMATION_SET_H_
