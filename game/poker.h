/* poker.h:
 * This file defines some datatypes as an object-oriented wrapper for
 * the poker_value interface. */
#ifndef POKER_H_
#define POKER_H_

#include <assert.h>
#include <stdint.h>
#include <array>
#include <initializer_list>
#include "card.h"
/*
 * A hand is a 52 bit value of its card values
 * Bit k is 1 if card value k is in the hand
 */
class Hand {
public:
  inline Hand() : cards_(0) {}
  inline explicit Hand(uint64_t c) : cards_(c) {}
  inline explicit Hand(Card c) { cards_ = (1ULL << c.value()); }
  inline explicit Hand(std::initializer_list<Card> c) : cards_(0) {
    AddCards(c);
  }
  inline int8_t Lowest() const {
    for (int8_t i = 0; i < 52; ++i) { if (Contains(Card{i})) { return i; } }
    return -1;
  }
  inline int8_t SecondLowest() const {
    //cards_ without lowest bit
    uint64_t c = cards_ & (cards_ - 1);
    for (int8_t i = 0; i < 52; ++i) { if (c & (1ULL << i)) { return i; } }
    return -1;
  }
  static Hand First(int size) {
    assert(size > 0);
    return Hand((1ULL<<size)-1);
  }
  inline bool Contains(const Card c) const {
    return cards_ & (1ULL<<c.value());
  }
  inline void AddCard(const Card c) { cards_ |= (1ULL << c.value()); }
  inline void AddCards(std::initializer_list<Card> c) {
    for (auto it = begin(c); it != end(c); ++it) {
      AddCard(*it);
    }
  }
  inline Hand Union(const Hand h) const { return Hand(cards_ | h.cards()); }
  inline uint64_t cards() const { return cards_; }
  inline void set_cards(uint64_t c) { cards_ = c; }
  uint64_t Value() const; //higher value -> wins showdown

  /* ValueWith evaluates a two card hand with respect to a board h, or
   * a board with respect to a two-card hand h.
   * TODO: Implement this in value.cc for 10x speedup */
  uint64_t ValueWith(const Hand h) const; //higher value -> wins showdown
  uint64_t ValueWithSeven(const Hand h) const; //higher value -> wins showdown

  /* Shift cards values in the deck so they are unique relative to h. This
   * is useful if you are iterating over pairs of hands with NextHand;
   * you can choose the first hand from a 52 card deck, and the second
   * hand from a 50 card deck which is then expanded by the first
   * hand. */
  Hand Expand(const Hand h) const;
  /* Pick a different hand of the same number of cards, with deck_size
   * as the max card number. */
  bool NextHand(int deck_size);

private:
  uint64_t cards_;
};

std::pair<std::array<int8_t, 1326>*, std::array<int8_t, 1326>*> GetIndexToCardTables();
std::array<uint64_t, 1326>* GetIndexToHandTable();
std::array<int, 52*51>* GetCardsToIndexTable();
std::array<int, 6> GetHandsIn(int hand_index_1, int hand_index_2);
/* Turns Cards into the index of the hand they create */
int CardsToIndex(int8_t c1, int8_t c2);
/* For threaded use, the initialize functions are not threadsafe */
void HandIndexTablesInitialize();

#endif //POKER_H_
