#include "pocket_bucket.h"
#include "../montecarlo/montecarlo.h"
namespace abstract_game {

PocketBucket BucketPocket(Hand pocket, Hand board, int num_cards, int num_players, int max_buckets) {
  MontecarloEvaluator eval = MontecarloEvaluator(pocket, board, num_cards, num_players);

  double wr = eval.RunIterationsByCount(100);

  /* subtract eps so that we never return max_buckets, because then we
   * would have max_buckets+1 different buckets when we include 0 */
  return static_cast<PocketBucket>(floor((wr * max_buckets) - 0.000001));
}
pocket_bucket_by_pocket_t AllBuckets(Hand board, int num_cards, int num_players) {
  /* don't need to value initialize because we set every index */
  pocket_bucket_by_pocket_t frame;
  std::array<uint64_t,1326> &ith = *(GetIndexToHandTable());
  for (int hand_index = 0; hand_index < 1326; ++hand_index) {
    Hand h{ith[hand_index]};
    frame[hand_index] = BucketPocket(h, board, num_cards, num_players, MAX_HAND_BUCKETS);
  }
  return frame;
}
} // namespace abstract_game
