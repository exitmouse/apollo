#include "poker.h"

#include <stdio.h>
#include "value.h"

namespace {
  std::array<int8_t, 1326> *index_to_first_card;
  std::array<int8_t, 1326> *index_to_second_card;
  bool itc_init = false;
  std::array<uint64_t, 1326> *index_to_hand;
  std::array<int, 52*51> *cards_to_index;
  bool ith_init = false;
  void ItcInit() {
    assert(!itc_init);
    index_to_first_card = new std::array<int8_t, 1326>();
    index_to_second_card = new std::array<int8_t, 1326>();
    int index = 0;
    /* unsafe loop, placing continues inside desyncs index and h. Be
     * careful if you change this code */
    for (Hand h = Hand::First(2); true; ++index) {
      (*index_to_first_card)[index] = h.Lowest();
      (*index_to_second_card)[index] = h.SecondLowest();
      if (!h.NextHand(52)) { break; }
    }
    itc_init = true;
  }
  void IthInit() {
    assert(!ith_init);
    index_to_hand = new std::array<uint64_t, 1326>();
    cards_to_index = new std::array<int, 52*51>();
    int index = 0;
    /* unsafe loop, placing continues inside desyncs index and h. Be
     * careful if you change this code */
    for (Hand h = Hand::First(2); true; ++index) {
      (*index_to_hand)[index] = h.cards();
      (*cards_to_index)[h.Lowest() + 52*(h.SecondLowest() - 1)] = index;
      if (!h.NextHand(52)) { break; }
    }
    ith_init = true;
  }
} // namespace

void HandIndexTablesInitialize() {
  ItcInit();
  IthInit();
}

std::pair<std::array<int8_t, 1326>*, std::array<int8_t, 1326>*> GetIndexToCardTables() {
  if (!itc_init) {
    ItcInit();
  }
  return std::make_pair(index_to_first_card, index_to_second_card);
}
std::array<uint64_t, 1326>* GetIndexToHandTable() {
  if (!ith_init) {
    IthInit();
  }
  return index_to_hand;
}
std::array<int, 52*51>* GetCardsToIndexTable() {
  if (!ith_init) {
    IthInit();
  }
  return cards_to_index;
}
int CardsToIndex(int8_t c1, int8_t c2) {
  assert(c1 != c2);
  if (c2 > c1) {
    return (*GetCardsToIndexTable())[c1 + 52*(c2-1)];
  } else {
    return (*GetCardsToIndexTable())[c2 + 52*(c1-1)];
  }
}
std::array<int, 6> GetHandsIn(int hand_index_1, int hand_index_2) {
  uint64_t h_1 = (*GetIndexToHandTable())[hand_index_1];
  uint64_t h_2 = (*GetIndexToHandTable())[hand_index_2];
  assert((h_1 & h_2) == 0);
  int8_t c1 = Hand{h_1}.Lowest();
  int8_t c2 = Hand{h_1}.SecondLowest();
  int8_t c3 = Hand{h_2}.Lowest();
  int8_t c4 = Hand{h_2}.SecondLowest();
  assert(c1 != -1);
  assert(c2 != -1);
  assert(c3 != -1);
  assert(c4 != -1);
  return std::array<int, 6>{
    CardsToIndex(c1, c2),
    CardsToIndex(c1, c3),
    CardsToIndex(c1, c4),
    CardsToIndex(c2, c3),
    CardsToIndex(c2, c4),
    CardsToIndex(c3, c4)};
}

/* Sets cards to the next possible hand (for iterating). Returns false if we have reached the last hand */
bool Hand::NextHand(int deck_size) {
  //We use gosper's hack.
  const uint64_t lob = cards_ & -cards_; //low order bit
  const uint64_t r = cards_ + lob; //set the bit one higher than the lowest block of ones
  if (r == (1ULL << deck_size)) { return false; /*last one*/ }
  cards_ = (((r^cards_)/lob) >> 2) | r; //put rest of block of ones back at start
  return true;
}

Hand Hand::Expand(const Hand h) const {
  uint64_t h_cards = h.cards_;
  uint64_t me_cards = cards_;
  for (uint64_t lob = h_cards & -h_cards;
       h_cards -= lob, lob;
       lob = h_cards & -h_cards) {
    //cards below this value stay the same
    uint64_t lower = me_cards & (lob - 1);
    //cards above this value or at it move up 1
    me_cards = ((me_cards - lower) << 1) | lower;
  }
  return Hand(me_cards);
}

uint64_t Hand::Value() const {
  return poker_value::ValueFiveCardHand(cards_);
}

uint64_t Hand::ValueWith(const Hand h) const {
  uint64_t seven_hand = cards_ | h.cards_;
  uint64_t low_drop = seven_hand;
  /* Iterate over bits in seven_hand, discarding parts of low_drop
   * lower than the current bit, then iterate over bits in
   * low_drop. This gives you all ascending pairs of bits in
   * seven_hand, and we just have to evaluate seven_hand without each
   * pair. This is the naive method. TODO: write ValueSevenCards in
   * value.cc */
  uint64_t max_value = 0;
  for (uint64_t lob = low_drop & -low_drop;
       low_drop -= lob, lob;
       lob = low_drop & -low_drop) {
    uint64_t tmp = low_drop;
    for (uint64_t nb = tmp & -tmp;
         tmp -= nb, nb;
         nb = tmp & -tmp) {
      uint64_t val = poker_value::ValueFiveCardHand(seven_hand - lob - nb);
      if (val > max_value) {
        max_value = val;
      }
    }
  }
  return max_value;
}

uint64_t Hand::ValueWithSeven(const Hand h) const {
  return poker_value::ValueSevenCardHand(cards_ | h.cards());
}
