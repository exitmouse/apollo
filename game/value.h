/* value.h:
 * This file declares functions for evaluating poker hands stored in a
 * specific integer format.
 * A card is stored as an integer from 0 to 51:
 * The ranks are integers 0 through 12
 * 0 means 2
 * 1 means 3
 * 8 means 10
 * 9 means J
 * 12 means A
 *
 * A card suit is 0-4
 * 0 -> Clubs
 * 1 -> Diamonds
 * 2 -> Hearts
 * 3 -> Spades
 *
 * The card is stored as rank + 13*suit (0-51)
 * So a card's rank is its number % 13
 * and its suit is its number / 13.
 *
 * A hand is then a 52 bit integer, where bit k is 1 if card k is in
 * the hand. */
#ifndef VALUE_H_
#define VALUE_H_

#include <stdint.h>
#include <sys/mman.h> //mmap

namespace poker_value {

struct HandTables {
  /* pop_count is a lookup table for the pop count function, which takes
   * an integer and returns the number of 1 bits. Only stored for 13 bit
   * integers, because that's where we need it. Some cpus have a
   * popcount instruction, and it is sometimes slower than a lookup
   * table; it can be worth testing. On a typical run, computing
   * popcounts via this method is only about a fifth of the instruction
   * count. */
  uint16_t pop_count[(1<<13)];
  /* straight_index is a lookup table; straight_index[i] is the
   * rank of the high card of the straight if i contains a straight, and
   * 0 otherwise. For example, if i represents the wheel,
   * straight_index[i] = 3. */
  uint16_t straight_index[(1<<13)];
  /* top_part is a lookup table that gives the top five nonzero bits
   * of a seven-card hand by removing the lowest two bits. This way it
   * can be used to get the top four nonzero bits of a six-card
   * tiebreaker set, or the top three of a five-card set etc.
   * 0101110101001 -> 0101110100000,
   * 0101110000000 -> 0101000000000. */
  uint16_t top_part[(1<<13)];

  /* flush_tiebreaker is a lookup table that gives the top five
   * nonzero bits of the integer. This way, when applied to a suit
   * with 6 cards in it, it still returns the top five cards. */
  uint16_t flush_tiebreaker[(1<<13)];
};

struct EvalTables {
  /* lookup table: for a ranks vector i, multiplicities[i] is the
   * vector that has bit k at position k*2 and all other bits 0.
   * This means that
   * multiplicities[c] + multiplicities[d] +
   * multiplicities[h] + multiplicities[s]
   * is a list of the 2bit multiplicities of each rank in the hand.
   * You have to first exclude the possibility that there is a 4 of a
   * kind, or it will overflow badly. */
  uint32_t multiplicities[(1<<13)];
  /* A 2^28 MB lookup table, which turns a multiplicity vector into a
   * hand value. Doesn't work for 4-of-a-kind hands (or flushes,
   * because it doesn't take any suit information) */
  uint32_t multiplicity_values[(1<<26)];
};

/* Hand types for hand values.
 *
 * We define kStraightFlush = 9 just so that
 * kStraightFlush = kStraight + kFlush. */
constexpr uint64_t kStraightFlush = 9;
constexpr uint64_t kFourOfAKind = 7;
constexpr uint64_t kFullHouse = 6;
constexpr uint64_t kFlush = 5;
constexpr uint64_t kStraight = 4;
constexpr uint64_t kThreeOfAKind = 3;
constexpr uint64_t kTwoPair = 2;
constexpr uint64_t kOnePair = 1;
constexpr uint64_t kNoPair = 0;


/* This function takes c, a hand which must have five cards, and
 * returns an integer v(c). We guarantee that if c wins in a showdown
 * against another hand d, v(c) > v(d).*/
uint64_t ValueFiveCardHand(uint64_t c);

/* As ValueFiveCardHand, but requires a seven-card hand and gives the
 * best value of a five card subset. The values from this function
 * cannot be compared to the values from ValueFiveCardHand. */
uint64_t ValueSevenCardHand(uint64_t c);

/* Should compute the same values as ValueSevenCardHand but faster. */
uint64_t Eval7(uint64_t c);

//Helper function we use to make values for hands.
uint64_t ValueFrom(uint64_t type, uint64_t grouped_cards, uint64_t remainder_cards);

//Return pointers to the lookup tables
HandTables *GetHandTables();
EvalTables *GetEvalTables();
void InitEvalTables();

} // namespace poker_value

#endif //VALUE_H_
