/* card.h
 * This file defines the card type, and gives helper functions. */
#ifndef CARD_H_
#define CARD_H_
#include "stdint.h"
#include "assert.h"
#include <random>

class Card {
public:
  Card(): value_(0) {}
  explicit Card(int8_t v) { assert(0 <= v); assert(v < 52); value_ = v; }
  Card(int8_t r, int8_t s) { value_ = r + 13*s; }
  int8_t value() const { return value_; }
  void set_value(int8_t v) { value_ = v; }
  int8_t Rank() const { return value_ % 13; }
  int8_t Suit() const { return value_ / 13; }
  static uint64_t RandomCard(uint64_t used_cards);
  static Card RandomCardProper(uint64_t used_cards);

  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & value_;
  }
private:
  int8_t value_;
  static inline int rand_() { 
  	return  rand() % 52;
  }

};

bool operator==(const Card& lhs, const Card& rhs);
#endif //CARD_H_
