#include "public_information_set.h"
#include <iterator> //begin, end
#include "../bot/game_abstracter.h"
#include "math.h"
#include "../rng.h"
namespace {
  GamePhase PhaseFromCardsSeen(int cards_seen) {
    switch (cards_seen) {
      case 0: {
        return GamePhase::PRE_FLOP;
      }
      case 1:
      case 2:
      case 3: {
        return GamePhase::ON_FLOP;
      }
      case 4: {
        return GamePhase::ON_TURN;
      }
      case 5: {
        return GamePhase::ON_RIVER;
      }
      default: {
        assert(false);
      }
    }
  }
}

bool PublicInformationSet::BettingRoundDone() const {
  if (players_left() == 1) { return true; }
  for (int i = 0; i < num_players; ++i) {
    /* If we haven't folded, haven't gone all-in, and our bet doesn't
     * match the current bet, we are not done betting. */
    if (has_folded[i]) { continue; }
    if (total_amount_in_pot(i) < std::min(stack_sizes[i], current_bet)) { return false; }
  }
  //must make it around the circle at least once each round
  if (!button_has_acted) {
    return false;
  }
  /* It's the big blind's turn pre-flop and we think betting might be
   * done, but she gets a chance to raise */
  
  if (turn_index == big_blind_index() && current_bet == big_blind && cards_seen == 0) {
    return false;
  } else {
    return true;
  }
}

bool PublicInformationSet::IsValid(Fold) const {
  if (cards_seen > 0 && cards_seen < 3) { return false; }
  if (BettingRoundDone()) { return false; }
  if (!has_folded[turn_index] && cards_seen == 0 && current_bet < big_blind) { return false; } //ante up
  return true;
}
bool PublicInformationSet::IsValid(Check) const {
  if (cards_seen > 0 && cards_seen < 3) { return false; }
  if (BettingRoundDone()) { return false; }
  if (has_folded[turn_index]) { return false; }
  if (betting_opened) {
    if (current_bet == big_blind && turn_index == big_blind_index() && cards_seen == 0) {
      return true;
    }
    return false;
  }
  return true;
}
bool PublicInformationSet::IsValid(Call c) const {
  if (cards_seen > 0 && cards_seen < 3) { return false; }
  if (BettingRoundDone()) { return false; }
  if (has_folded[turn_index]) { return false; }
  if (current_bet == big_blind && turn_index == big_blind_index() && cards_seen == 0) {
    return false;
  }
  if (c.amt + last_round_bet == std::min(stack_sizes[turn_index], current_bet)) { return true; }
  return false;
}
bool PublicInformationSet::IsValid(Post p) const {
  //TODO if small blind is >1 and someone doesn't post the full small blind
  if (current_bet == 1 && p.amt == 1) {
    if (stack_sizes[turn_index] == 1) {
      return true;
    }
  } else if (current_bet == 0 && p.amt == big_blind/2) {
    return true; //small blind
  } else if (current_bet == big_blind/2 && p.amt == big_blind) {
    return true; //big blind
  }
  return false;
}
bool PublicInformationSet::IsValid(Bet b) const {
  if (cards_seen > 0 && cards_seen < 3) { return false; }
  if (BettingRoundDone()) { return false; }
  if (has_folded[turn_index]) { return false; }
  if (b.amt + last_round_bet > stack_sizes[turn_index]) { return false; }
  //minimum bet
  if (b.amt < minimum_raise()) {
    if (b.amt == stack_sizes[turn_index] - last_round_bet) {
      return true;
    }
    else {
      return false;
    }
  }
  //pot limit
  if (b.amt > maximum_raise()) { return false; }
  if (!betting_opened) {
    return true;
  } else {
    return false;
  }
}
bool PublicInformationSet::IsValid(Raise r) const {
  if (cards_seen > 0 && cards_seen < 3) { return false; }
  if (BettingRoundDone()) { return false; }
  if (has_folded[turn_index]) { return false; }
  if (r.amt + last_round_bet > stack_sizes[turn_index]) { return false; }
  if (r.amt == call_amount_this_round()) { return false; }
  //minimum raise
  if (r.amt < minimum_raise()) { return false; }
  //pot limit
  if (r.amt > maximum_raise()) { return false; }
  if (!betting_opened) {
    return false;
  } else {
    return true;
  }
}
bool PublicInformationSet::IsValid(Card) const {
  if (players_left() == 1) { return false; }
  if (cards_seen > 0 && cards_seen < 3) { return true; }
  if (BettingRoundDone() && cards_seen < 5) { return true; }
  return false;
}

int PublicInformationSet::players_left() const {
  int i = num_players;
  for (auto it = begin(has_folded); it != end(has_folded); ++it) {
    if (*it) {
      --i;
    }
  }
  return i;
}

void PublicInformationSet::Refund() {
  if (players_left() == num_players) {
    //TODO: can a bot bet more than is rational (is going all-in when
    //you have the highest stack legal?)
    return;
  }
  int max_stack = 0;
  int second_max_stack = 0;
  int arg_max = 0;
  for (int i = 0; i < 3; ++i) {
    if (!has_folded[i] && max_stack > 0) {
      if (stack_sizes[i] > max_stack) {
        second_max_stack = max_stack;
        max_stack = stack_sizes[i];
        arg_max = i;
      } else {
      second_max_stack = stack_sizes[i];
      }
    } else {
      max_stack = stack_sizes[i];
      arg_max = i;
    }
  }
  if (current_bet > second_max_stack && current_bet >> big_blind) {
    pot -= current_bet - second_max_stack;
    betting_round_pot -= current_bet - second_max_stack;
    current_bet = second_max_stack;
    betting_round_amount_in_pot[arg_max] = second_max_stack - past_amount_in_pot[arg_max];
  }
}

void PublicInformationSet::UpdatePocketBuckets() {
  buckets_by_pocket = abstract_game::AllBuckets(board, cards_seen, players_left());
}

void PublicInformationSet::operator()(Fold f) {
  assert(IsValid(f));
  has_folded[turn_index] = true;
  Refund();
  UpdatePocketBuckets();
  NextTurnIndex();
}
void PublicInformationSet::operator()(Check c) {
  assert(IsValid(c));
  NextTurnIndex();
}
void PublicInformationSet::BetToWithinRound(int amt) {
  pot += amt - betting_round_amount_in_pot[turn_index];
  betting_round_pot += amt - betting_round_amount_in_pot[turn_index];
  betting_round_amount_in_pot[turn_index] = amt;
}
void PublicInformationSet::operator()(Call c) {
  assert(IsValid(c));
  BetToWithinRound(c.amt);
  NextTurnIndex();
}
void PublicInformationSet::PutInPotTo(int amt) {
  BetToWithinRound(amt);
  //amt - this_round_bet
  minimum_bet_delta = amt + last_round_bet - current_bet;
  current_bet = amt + last_round_bet;
}
void PublicInformationSet::operator()(Bet b) {
  assert(IsValid(b));
  betting_opened = true;
  PutInPotTo(b.amt);
  NextTurnIndex();
}
void PublicInformationSet::operator()(Post p) {
  assert(IsValid(p));
  betting_opened = true;
  if (current_bet == 1) {
    /* This code is run if p.amt == 1 or if it's == 2 */
    /* If the big blind is all-in, and posts less than the full big
     * blind, tournaments (and engine.jar) still use this "true pot"
     * method. */
    PutInPotTo(p.amt);
    minimum_bet_delta = 2;
    current_bet = 2;
  } else {
    PutInPotTo(p.amt);
  }
  NextTurnIndex();
}
void PublicInformationSet::operator()(Raise r) {
  assert(IsValid(r));
  if (r.amt == minimum_raise()) {
    ++num_min_raises;
  } else if (r.amt == fractional_pot_raise(2)) {
    ++num_half_raises;
  }
  PutInPotTo(r.amt);
  NextTurnIndex();
}
void PublicInformationSet::operator()(Card c) {
  //Nook at the cards
  assert(IsValid(c));
  ++cards_seen;
  board.AddCard(c);
  phase = PhaseFromCardsSeen(cards_seen);
  //even heads-up, bb acts first post-flop
  turn_index = Clockwise(button_index, 1);
  //Switch betting rounds (must be idempotent for flop)
  minimum_bet_delta = big_blind;
  last_round_bet = current_bet;
  betting_round_pot = 0;
  
  for (int i = 0; i < num_players; ++i) {
    past_amount_in_pot[i] += betting_round_amount_in_pot[i];
  }

  betting_round_amount_in_pot = std::vector<int>(num_players, 0);
  betting_opened = false;
  button_has_acted = false;
  UpdatePocketBuckets();
  while (!capped_actions_with_player.empty()) {
    PopToSummaryBets();
  }
}

void PublicInformationSet::PopToSummaryBets() {
  //TODO get real abstractified amounts out of abstracted actions
  AbstractAction next = capped_abstractified_actions.front();
  std::pair<Action,int> old = capped_actions_with_player.front();
  total_summary_bet_amounts[old.second] += ActionAmount(old.first);
  capped_actions_with_player.pop_front();
  capped_abstractified_actions.pop_front();
}

void PublicInformationSet::Do(Action a) {
  actions.push_back(a);
  Action absolute_form = Absolutify(a, last_round_bet);
  capped_actions_with_player.push_back(std::make_pair<>(absolute_form, turn_index));
  capped_abstractified_actions.push_back(StateTranslationStep(a));
  if (capped_actions_with_player.size() > abstract_game::ABSTRACT_ACTION_MEMORY) {
    PopToSummaryBets();
  }
  boost::apply_visitor(*this, a);
  //don't expect actions from folded players
  while (has_folded[turn_index]) {
    NextTurnIndex();
  }
}

AbstractAction PublicInformationSet::StateTranslationStep(Action a) const {
  std::vector<int> bins = { minimum_raise(), //m
                            fractional_pot_raise(2), //h
                            maximum_raise() }; //p
  return boost::apply_visitor(StateTranslator(bins), a);
}

/* Probabilistically return the index of i's nearest bin amount. Assumes bins is in sorted order. */
int PublicInformationSet::Tartanian5Bin(std::vector<int> bins, int x) {
  int high_index = 0;
  /* TODO could binary search */
  while (high_index < bins.size()) {
    if (bins[high_index] == x) {
      return high_index;
    } else if (bins[high_index] < x) {
      ++high_index;
    } else {
      break;
    }
  }
  int low_index = high_index - 1;
  assert(!(high_index == bins.size() || low_index < 0));
  int high = bins[high_index];
  int low = bins[low_index];
  /* Probability of returning L is
   * ((H-x)(1+L))/((H-L)(1+x)). Otherwise return H. */
  double numerator = static_cast<double>((high - x) * (1 + low));
  double denominator = static_cast<double>((high - low) * (1 + x));
  double p = numerator/denominator;
  std::bernoulli_distribution d(p);
  if(d(GetMersenneTwister())) {
    return low_index;
  } else {
    return high_index;
  }
}

/* The "Action" this accepts is abstract, this is a boost variant type
 * hack */
const PublicInformationSet* PublicInformationSet::ResultOf(Action a) const {
  PublicInformationSet* result = new PublicInformationSet(*this);
  result->Do(boost::apply_visitor<>(ActionTranslator{result},a));
  return result;
}

/* Traverses the actions in order, validating and building aggregate
 * values.*/
void PublicInformationSet::DoActions(std::vector<Action> a) {
  for (auto it = begin(a); it != end(a); ++it) {
    //nike
    Do(*it);
  }
}

