#ifndef HAND_OUTCOME_H_
#define HAND_OUTCOME_H_

#include "poker.h"

/* The result of a hand, for use in combo with a
 * PublicInformationSet. Does not contain information on the pot, or
 * on who bet how much money; this is just enough info to determine
 * the winner of the hand given a PublicInformationSet. Indices are by
 * the same convention as PublicInformationSet: 0 is us. TODO put
 * code in PublicInformationSet to determine who gets what amount of
 * money given a HandOutcome (because of side pots, it's complicated,
 * but I've written similar code before) */
struct HandOutcome {
  bool has_folded[3]{}; //zero initialized to false
  Hand showdown_hands[3]{}; //zero if not in showdown
}

#endif // HAND_OUTCOME_H_
