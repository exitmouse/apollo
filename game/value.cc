#include "value.h"
#include <assert.h>
#include <algorithm> // max

namespace poker_value {

namespace {
bool hand_tables_init = false;
bool eval_tables_init = false;
HandTables *hand_tables;
EvalTables *eval_tables;
void PopCountInit();
void StraightIndexInit();
void TopPartInit();
void FlushTiebreakerInit();
void MultiplicitiesInit();
void MultiplicityValuesInit();
/* Returns the value of the flushes in the hand, or zero */
uint64_t FlushesOrZero(const uint64_t c, const uint64_t d, const uint64_t h, const uint64_t s) __attribute__((always_inline));
/* Create a canonical hand from a set of rank multiplicities. */
uint64_t FromMultiplicities(const int8_t mults[13]);
  
void HandTablesInit() {
  hand_tables = new HandTables();
  PopCountInit();
  StraightIndexInit();
  TopPartInit();
  FlushTiebreakerInit();
  hand_tables_init = true;
}

void EvalTablesInit() {
  /* Must initialize handtables before this, because it computes
   * ValueSevenCardHand */
  if (!hand_tables_init) {
    HandTablesInit();
  }
  eval_tables = new EvalTables();
  MultiplicitiesInit();
  MultiplicityValuesInit();
  eval_tables_init = true;
}

void PopCountInit() {
  for (uint16_t i = 0; i < (1<<13); ++i) {
    for (uint16_t bits = i; bits != 0; bits >>= 1) {
      hand_tables->pop_count[i] += bits & 1;
    }
  }
}

void StraightIndexInit() {
  for (uint16_t i = 0; i < (1<<13); ++i) {
    if ((i & ((1 << 12) | 0xf)) == ((1 << 12) | 0xf)) { // the wheel
      hand_tables->straight_index[i] = 3;
    }
    //Move the mask up and test if those five bits are set
    const uint16_t straight_mask = 0x1f;
    for (uint16_t j = 0; j < 9; ++j) { //8 = 13 - 5 set bits
      if ((i & (straight_mask << j)) == (straight_mask << j)) { 
        hand_tables->straight_index[i] = j+4;
      }
    }
  }
}

void TopPartInit() {
  for (uint16_t i = 0; i < (1<<13); ++i) {
    uint16_t lob = i & -i;
    uint16_t high_part = i - lob;
    hand_tables->top_part[i] = high_part - (high_part & -high_part);
  }
}

void FlushTiebreakerInit() {
  for (uint16_t i = 0; i < (1<<13); ++i) {
    uint16_t top_five = 0;
    for (int j = 12; j >= 0; --j) { //must be signed integer so loop terminates
      top_five += i & (1 << j);
      if(hand_tables->pop_count[top_five] == 5) {
        break;
      }
    }
    hand_tables->flush_tiebreaker[i] = top_five;
  }
}

void MultiplicitiesInit() {
  for (uint16_t i = 0; i < (1<<13); ++i) {
    for (int shift = 0; shift < 13; ++shift) {
      if (i & (1 << shift)) {
        /* we leftshift by shift*2 because each multiplicity is 0-3
         * and takes two bits. */
        eval_tables->multiplicities[i] += (1 << (shift*2));
      }
    }
  }
}

//Monster function, sorry.
void MultiplicityValuesInit() {
  /* We need to compute the value once for each multiplicity
   * equivalence class. We simply pick all combinations of seven
   * different ranks, rejecting ones with five or more in a single
   * rank. This makes the loop run 13^7 times, which is ~60,000,000
   * and fine. Since poker-eval takes about 50 ms/10^6 hands (on my
   * box), this function should crunch for about 3 seconds. */
  for (int c1 = 0; c1 < 13; ++c1) {
    for (int c2 = 0; c2 < 13; ++c2) {
      for (int c3 = 0; c3 < 13; ++c3) {
        for (int c4 = 0; c4 < 13; ++c4) {
          for (int c5 = 0; c5 < 13; ++c5) {
            for (int c6 = 0; c6 < 13; ++c6) {
              for (int c7 = 0; c7 < 13; ++c7) {
                int8_t mults[13]{}; //value initialize to zero
                ++mults[c1];
                ++mults[c2];
                ++mults[c3];
                ++mults[c4];
                ++mults[c5];
                ++mults[c6];
                ++mults[c7];
                //check array valid
                bool over_three = false;
                for (int rank = 0; rank < 13; ++rank) {
                  if (mults[rank] > 3) {
                    over_three = true;
                    break;
                  }
                }
                if (over_three) {
                  continue;
                }
                uint64_t cards = FromMultiplicities(mults);
                uint32_t i = 0;
                i += (1 << (c1 * 2));
                i += (1 << (c2 * 2));
                i += (1 << (c3 * 2));
                i += (1 << (c4 * 2));
                i += (1 << (c5 * 2));
                i += (1 << (c6 * 2));
                i += (1 << (c7 * 2));
                eval_tables->multiplicity_values[i] = static_cast<uint32_t>(ValueSevenCardHand(cards));
              }
            }
          }
        }
      }
    }
  }
}

  __attribute__((always_inline)) uint64_t FlushesOrZero(const uint64_t c, const uint64_t d, const uint64_t h, const uint64_t s) {
  //check if we have a flush. Five cards in one suit means a maximum
  //of two cards outside it, for a maximum of two pairs; four of a
  //kind and full house are then impossible so we can check for a
  //flush first.
  uint16_t const *pops = GetHandTables()->pop_count;
  for (const uint64_t suit : {c, d, h, s}) {
    if (pops[suit] >= 5) {
      uint16_t const *straight = GetHandTables()->straight_index;
      //check if we have a straight flush, a straight all in one suit.
      if (straight[suit]) {
        return ValueFrom(kStraightFlush, 0, straight[suit]);
      } else {
        //Without flush_tiebreaker, cards not used in the hand will be used for tiebreaking.
        uint16_t const *flush_tiebreaker = GetHandTables()->flush_tiebreaker;
        return ValueFrom(kFlush, 0, flush_tiebreaker[suit]);
      }
    }
  }
  return 0;
}

uint64_t FromMultiplicities(const int8_t mults[13]) {
  uint64_t cards = 0;
  for (int8_t rank = 0; rank < 13; rank++) {
    switch (mults[rank]) {
      case 4: {
        cards |= (1ULL << (rank + 39));
        [[clang::fallthrough]];
      }
      case 3: {
        cards |= (1ULL << (rank + 26));
        [[clang::fallthrough]];
      }
      case 2: {
        cards |= (1ULL << (rank + 13));
        [[clang::fallthrough]];
      }
      case 1: {
        cards |= (1ULL << (rank));
      }
    }
  }
  return cards;
}
} // namespace

HandTables *GetHandTables() {
  if (!hand_tables_init) {
    HandTablesInit();
  }
  return hand_tables;
}
EvalTables *GetEvalTables() {
  if (!eval_tables_init) {
    EvalTablesInit();
  }
  return eval_tables;
}
/* exported */
void InitEvalTables() {
  if (!eval_tables_init) {
    EvalTablesInit();
  }
}

/* We have 64 bits in our hand value. We offset the handtype by 26 so
 * that we can hold two subsets of the ranks (which take 13 bits to
 * express naively) in the tiebreaker data below the handtype, without
 * any processing. Since the highest hand type kStraightFlush = 9 =
 * 0b1001 which is four bits, the hand value still fits in a 32 bit
 * integer. */
uint64_t ValueFrom(uint64_t type,
                                uint64_t grouped_cards,
                                uint64_t remainder_cards) {
  assert(grouped_cards < (1 << 13));
  assert(remainder_cards < (1 << 13));
  uint64_t value = (type << 26);
  value += (grouped_cards << 13);
  value += remainder_cards;
  return value;
}

uint64_t ValueFiveCardHand(uint64_t const cards) {
  //get the distinct ranks of cards
  const uint64_t c = (cards / (1ULL << 0))  & ((1ULL << 13) - 1ULL); //clubs
  const uint64_t d = (cards / (1ULL << 13)) & ((1ULL << 13) - 1ULL); //diamonds
  const uint64_t h = (cards / (1ULL << 26)) & ((1ULL << 13) - 1ULL); //hearts
  const uint64_t s = (cards / (1ULL << 39)) & ((1ULL << 13) - 1ULL); //spades
  const uint64_t ranks = c | d | h | s;
  //check if we have a flush
  uint64_t hand_type = 0;
  uint16_t const *pops = GetHandTables()->pop_count;
  if (pops[c] == 5 || pops[d] == 5 || pops[h] == 5 || pops[s] == 5) {
    hand_type += kFlush;
  }
  //check if we have a straight
  const uint64_t lob = ranks & -ranks;
  //0b11111, 5 consecutive
  if (ranks/lob == 0x1f) {
    hand_type += kStraight;
  }
  //the wheel straight (ace low) needs to be scored separately
  if (ranks == ((1ULL << 12) | 0xf)) {
    hand_type += kStraight;
    return ValueFrom(hand_type, 0, 0); //lowest possible
  }
  //if both happen, we get kStraight + kFlush = kStraightFlush
  if (hand_type != 0) {
    /* If we had a straight or a flush we're done.
     * We need to add tiebreaker bits, but we're lucky because a
     * straight or flush is higher than another straight or flush
     * when the top card is higher, or if there's a tie then if the
     * next top card is higher, etc. So one is higher than another if
     * the 13 bits of ranks are higher.
     */
    return ValueFrom(hand_type, ranks, 0);
  }
  //# of distinct cards in a 13 bit hand:
  switch (pops[ranks]) {
  case 5: { //0 pair
    return ValueFrom(kNoPair, 0, ranks);
  }
  case 4: { //1 pair
    uint64_t pair = (c & d) | (c & h) | (c & s) | (d & h) | (d & s) | (h & s);
    assert(pair != 0);
    return ValueFrom(kOnePair, pair, ranks-pair);
  }
  case 3: { //2 pair or 3 of a kind
    uint64_t pair = (c & d) | (c & h) | (c & s) | (d & h) | (d & s) | (h & s);
    assert(pair > 0);
    assert(pair < (1<<13));
    if (pops[pair] == 1) { //only one repeated card, 3 of a kind
      return ValueFrom(kThreeOfAKind, pair, ranks-pair);
    }
    else { //2 pair
      return ValueFrom(kTwoPair, pair, ranks-pair);
    }
    assert(false);
  }
  case 2: { //4 of a kind or full house
    uint64_t quad = c & d & h & s;
    if (quad) { //4 of a kind
      return ValueFrom(kFourOfAKind, quad, ranks-quad);
    }
    else { //full house
      uint64_t trip = (c & d & h) | (c & d & s) | (c & h & s) | (d & h & s);
      assert(trip > 0);
      /* The tiebreaker is just the tripled card. */
      return ValueFrom(kFullHouse, trip, ranks-trip);
    }
    assert(false);
  }
  default:
    assert(false); //Wrong number of cards in hand
  }
  assert(false);
}

uint64_t ValueSevenCardHand(uint64_t const cards) {
  //get the distinct ranks of cards
  const uint64_t c = (cards / (1ULL << 0))  & ((1ULL << 13) - 1ULL); //clubs
  const uint64_t d = (cards / (1ULL << 13)) & ((1ULL << 13) - 1ULL); //diamonds
  const uint64_t h = (cards / (1ULL << 26)) & ((1ULL << 13) - 1ULL); //hearts
  const uint64_t s = (cards / (1ULL << 39)) & ((1ULL << 13) - 1ULL); //spades

  uint64_t flush_val = FlushesOrZero(c,d,h,s);
  if (flush_val) {
    return flush_val;
  }

  const uint64_t ranks = c | d | h | s;
  //check if we have a straight
  uint16_t const *straight = GetHandTables()->straight_index;
  if (straight[ranks]) {
    return ValueFrom(kStraight, 0, straight[ranks]);
  }
  /* pops[rank] is the number of distinct ranks in the hand. Since
   * we're down to the hand types involving pairings, the number of
   * hand types we have to think about in case i is the number of
   * partitions of 7 of length i with each component less than or
   * equal to 4 (the components are the numbers of cards in each rank)
   * */
  uint16_t const *pops = GetHandTables()->pop_count;
  uint16_t const *top_part = GetHandTables()->top_part;
  switch (pops[ranks]) {
  case 7: { //0 pair
    return ValueFrom(kNoPair, 0, top_part[ranks]);
  }
  case 6: { //1 pair
    uint64_t pair = (c & d) | (c & h) | (c & s) | (d & h) | (d & s) | (h & s);
    assert(pair != 0);
    return ValueFrom(kOnePair, pair, top_part[ranks-pair]);
  }
  case 5: { //2 pair or 3 of a kind
    uint64_t pair = (c & d) | (c & h) | (c & s) | (d & h) | (d & s) | (h & s);
    assert(pair > 0);
    assert(pair < (1<<13));
    if (pops[pair] == 1) { //only one repeated card, 3 of a kind
      return ValueFrom(kThreeOfAKind, pair, 0);
    }
    else { //2 pair
      return ValueFrom(kTwoPair, pair, top_part[ranks-pair]);
    }
    assert(false);
  }
  case 4: { //4 of a kind, full house, or 3 pair
    uint64_t quad = c & d & h & s;
    if (quad) { //4 of a kind
      return ValueFrom(kFourOfAKind, quad, 0);
    }
    uint64_t pair = (c & d) | (c & h) | (c & s) | (d & h) | (d & s) | (h & s);
    assert(pair > 0);
    assert(pair < (1<<13));
    if (pops[pair] == 3) { //three repeated cards, end up with 2 pair
      uint64_t top_two_pairs = pair - (pair & -pair);
      uint64_t highest_remaining = std::max(pair & -pair, ranks - pair);
      return ValueFrom(kFourOfAKind, top_two_pairs, highest_remaining);
    } else { //full house
      uint64_t trip = (c & d & h) | (c & d & s) | (c & h & s) | (d & h & s);
      assert(trip > 0);
      return ValueFrom(kFullHouse, trip, 0);
    }
    assert(false);
  }
  case 3: { //4 of a kind + pair, full house + pair, double trips
    uint64_t trip = (c & d & h) | (c & d & s) | (c & h & s) | (d & h & s);
    if (pops[trip] == 1) { //4 of a kind + pair
      /* 4 of a kind is unique, so we need no tiebreaker other than its
       * rank. */
      return ValueFrom(kFourOfAKind, trip, 0);
    } else { //double trips OR full house + pair
      /* We can't accidentally break a tie here that we shouldn't
       * because only one person can have 3 of a kind in a given
       * rank, so if trip1 > trip2, the higher card of trip1 is bigger
       * than the higher card of trip2.*/
      return ValueFrom(kFullHouse, trip, 0);
    }
    assert(false);
  }
  case 2: { //4 of a kind + 3 of a kind.
    uint64_t quad = c & d & h & s;
    return ValueFrom(kFourOfAKind, quad, 0);
  }
  default:
    assert(false); //Wrong number of cards in hand
  }
  assert(false);
}

uint64_t Eval7(const uint64_t cards) {
  //get the distinct ranks of cards
  const uint64_t c = (cards / (1ULL << 0))  & ((1ULL << 13) - 1ULL); //clubs
  const uint64_t d = (cards / (1ULL << 13)) & ((1ULL << 13) - 1ULL); //diamonds
  const uint64_t h = (cards / (1ULL << 26)) & ((1ULL << 13) - 1ULL); //hearts
  const uint64_t s = (cards / (1ULL << 39)) & ((1ULL << 13) - 1ULL); //spades
  uint64_t flush_val = FlushesOrZero(c, d, h, s);
  if (flush_val) {
    return flush_val;
  }
  //check for four of a kind
  uint64_t quad = c & d & h & s;
  if (quad) {
    //there can be at most one four of a kind
    return ValueFrom(kFourOfAKind, quad, 0);
  }
  const uint32_t *multiplicities = GetEvalTables()->multiplicities;
  const uint32_t *multiplicity_values = GetEvalTables()->multiplicity_values;
  uint32_t multiplicity_vector = multiplicities[c] + \
                                 multiplicities[d] + \
                                 multiplicities[h] + \
                                 multiplicities[s];
  return multiplicity_values[multiplicity_vector];
}

} // namespace poker_value
