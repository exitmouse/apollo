#include "action.h"
#include <sstream>

bool operator==(const Fold&, const Fold&) {
  return true;
}
bool operator==(const Check&, const Check&) {
  return true;
}
bool operator==(const Call& lhs, const Call& rhs) {
  return lhs.amt == rhs.amt;
}
bool operator==(const Bet& lhs, const Bet& rhs) {
  return lhs.amt == rhs.amt;
}
bool operator==(const Raise& lhs, const Raise& rhs) {
  return lhs.amt == rhs.amt;
}
bool operator==(const Post& lhs, const Post& rhs) {
  return lhs.amt == rhs.amt;
}
bool operator==(const SummaryBet& lhs, const SummaryBet& rhs) {
  return lhs.amt == rhs.amt;
}

class Absolutifier : public boost::static_visitor<Action> {
public:
  Absolutifier(int i) : offset(i) {}
  int offset;
  Action operator()(const Fold o) const { return o; }
  Action operator()(const Check o) const { return o; }
  Action operator()(const Card o) const { return o; }
  Action operator()(const Call o) const {
    Call p{o.amt + offset};
    return p;
  }
  Action operator()(const Bet o) const {
    Bet p{o.amt + offset};
    return p;
  }
  Action operator()(const Raise o) const {
    Raise p{o.amt + offset};
    return p;
  }
  Action operator()(const Post o) const {
    Post p{o.amt + offset};
    return p;
  }
};

class BetRaiseUnMinner : public boost::static_visitor<AbstractAction> {
public:
  BetRaiseUnMinner() {}
  AbstractAction operator()(const Fold o) const { return o; }
  AbstractAction operator()(const Check o) const { return o; }
  AbstractAction operator()(const Card o) const { return o; }
  AbstractAction operator()(const Call o) const { return o; }
  AbstractAction operator()(const Post o) const { return o; }
  AbstractAction operator()(const SummaryBet o) const { return o; }
  AbstractAction operator()(const Bet o) const {
    if (o.amt == 0) { return Bet{1}; }
    else { return o; }
  }
  AbstractAction operator()(const Raise o) const {
    if (o.amt == 0) { return Raise{1}; }
    else { return o; }
  }
};

class AmountGetter : public boost::static_visitor<int> {
public:
  AmountGetter() {}
  int operator()(const Fold) const { return 0; }
  int operator()(const Check) const { return 0; }
  int operator()(const Card) const { return 0; }
  int operator()(const Call o) const { return o.amt; }
  int operator()(const Bet o) const { return o.amt; }
  int operator()(const Raise o) const { return o.amt; }
  int operator()(const Post o) const { return o.amt; }
  int operator()(const SummaryBet o) const { return o.amt; }
};
/* can't use std::hash, not guaranteed to persist nicely */
class Hasher : public boost::static_visitor<hash_t> {
public:
  hash_t operator()(Fold) const {
    return (1 << 0);
  }
  hash_t operator()(Check) const {
    return (1 << 1);
  }
  hash_t operator()(Call) const {
    return (1 << 2);
  }
  hash_t operator()(Post o) const {
    hash_t h = (1 << 3);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Bet o) const {
    hash_t h = (1 << 4);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Raise o) const {
    hash_t h = (1 << 5);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Card c) const {
    hash_t h = (1 << 6);
    return h + static_cast<hash_t>(c.value() << 8);
  }
};

class AbstractHasher : public boost::static_visitor<hash_t> {
public:
  hash_t operator()(Fold) const {
    return (1 << 0);
  }
  hash_t operator()(Check) const {
    return (1 << 1);
  }
  hash_t operator()(Call) const {
    return (1 << 2);
  }
  hash_t operator()(Post o) const {
    hash_t h = (1 << 3);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Bet o) const {
    hash_t h = (1 << 4);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Raise o) const {
    hash_t h = (1 << 5);
    return h + static_cast<hash_t>(o.amt << 8);
  }
  hash_t operator()(Card c) const {
    hash_t h = (1 << 6);
    return h + static_cast<hash_t>(c.value() << 8);
  }
  hash_t operator()(SummaryBet b) const {
    hash_t h = (1 << 7);
    return h + static_cast<hash_t>(b.amt << 8);
  }
};

class AbstractStringer : public boost::static_visitor<std::string> {
public:
  std::string operator()(Fold) const {
    return "(Fold)";
  }
  std::string operator()(Check) const {
    return "(Check)";
  }
  std::string operator()(Call) const {
    return "(Call)";
  }
  std::string operator()(Post o) const {
    std::stringstream s;
    s << "(Post ";
    s << o.amt;
    s << ")";
    return s.str();
  }
  std::string operator()(Bet o) const {
    std::stringstream s;
    s << "(Bet ";
    s << o.amt;
    s << ")";
    return s.str();
  }
  std::string operator()(Raise o) const {
    std::stringstream s;
    s << "(Raise ";
    s << o.amt;
    s << ")";
    return s.str();
  }
  std::string operator()(SummaryBet o) const {
    std::stringstream s;
    s << "(SummaryBet ";
    s << o.amt;
    s << ")";
    return s.str();
  }
  std::string operator()(Card c) const {
    std::stringstream s;
    s << "(Card ";
    s << std::to_string(c.value());
    s << ")";
    return s.str();
  }
};

hash_t HashAction(Action a) {
  return boost::apply_visitor<>(Hasher(), a);
}

hash_t HashAbstractAction(AbstractAction a) {
  return boost::apply_visitor<>(AbstractHasher(), a);
}

std::string StringAbstractAction(AbstractAction a) {
  return boost::apply_visitor<>(AbstractStringer(), a);
}

Action Absolutify(Action a, int i) {
  return boost::apply_visitor<>(Absolutifier(i), a);
}

AbstractAction NoMinBetsOrRaises(AbstractAction a) {
  return boost::apply_visitor<>(BetRaiseUnMinner(), a);
}

int ActionAmount(AbstractAction a) {
  return boost::apply_visitor<>(AmountGetter(), a);
}
