#ifndef GAME_PHASE_H_
#define GAME_PHASE_H_

enum class GamePhase {
  PRE_GAME,
  PRE_HAND,
  PRE_FLOP,
  ON_FLOP,
  ON_TURN,
  ON_RIVER
};

#endif //GAME_PHASE_H_
