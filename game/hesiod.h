/* hesiod.h: This module aggregates statistics on opponents, across
 * all of the games we feed through it. Named after the greek poet
 * Hesiod, who wrote about the battles of the gods. */
#ifndef HESIOD_H_
#define HESIOD_H_

class Hesiod {
public:
  void StartGame(std::string opp1, std::string opp2);
  void Record(Action h) {
    hand_state.Do(h);
  }
  void RecordOutcomes(HandOutcome h) {}
private:
  /* the state of the current hand */
  PublicInformationSet this_hand_state{};
  std::unordered_map<std::string, PlayerStatistics> player_log{};
};

#endif // HESIOD_H_
