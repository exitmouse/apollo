#include "card.h"

bool operator==(const Card& lhs, const Card& rhs) { return lhs.value() == rhs.value(); }

/* RandomCard
 *
 * Required: 
 *   [0] used_cards: the 52 bit vector of cards that we can't draw from
 * 
 * Returns: a random card not in the used_cards vector as represented by a 52 bit vector
 */
uint64_t Card::RandomCard(uint64_t used_cards) {
  uint64_t random_card = 1ULL << rand_();
  while ( (used_cards & random_card) != 0) {
    random_card = 1ULL << rand_();
  }
  return random_card;
}

Card Card::RandomCardProper(uint64_t used_cards) {
  int8_t shift = static_cast<int8_t>(rand_());
  uint64_t random_card = 1ULL << shift;
  while ( (used_cards & random_card) != 0) {
    shift = static_cast<int8_t>(rand_());
    random_card = 1ULL << shift;
  }
  return Card(shift);
}
