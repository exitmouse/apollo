/* public_information_set.h:
 * An information set is a complete set of information available to a
 * player when she makes her decision. There are multiple game
 * histories which result in the same information set, because we
 * don't know our opponents' hole cards. Our decisions can only depend
 * on 1) chance, 2) the information here. In practice, they may depend
 * on less than all of the information here, because there are too
 * many possible information sets to run CFR on all of them. This is
 * the information of one hand; information you have about these
 * players from previous rounds is represented separately.
 */
#ifndef PUBLIC_INFORMATION_SET_H_
#define PUBLIC_INFORMATION_SET_H_

#include <algorithm>
#include <initializer_list>
#include <vector>
#include <list>
#include "boost/variant.hpp" //visitor
#include "action.h"
#include "pocket_bucket.h"
#include "game_phase.h"
#include "poker.h"
#include <boost/serialization/array.hpp>
#include <memory>
 
struct ActionNode {
  explicit ActionNode(const Action a) : action(a), p(nullptr) {}
  ActionNode* and_then(const Action a) {
    ActionNode *node = new ActionNode{a};
    node->p = this;
    return node;
  }
  Action action;
  ActionNode *p;
};

class ActionSequence {
public:
  class const_iterator {
  public:
    typedef const Action value_type;
    typedef const Action& const_reference;
    typedef const Action* const_pointer;
    typedef std::forward_iterator_tag iterator_category;
    const_iterator() {}
    const_iterator(const ActionSequence *a) : ptr_(a->i_) {}
    const_iterator& operator++() {
      ptr_ = ptr_->p;
      return *this;
    }
    const_reference operator*() const {
      return ptr_->action;
    }
    bool operator==(const const_iterator& rhs) {
      return ptr_ == rhs.ptr_;
    }
    bool operator!=(const const_iterator& rhs) {
      return ptr_ == rhs.ptr_;
    }

  private:
    ActionNode *ptr_;
  };


  ActionSequence() : i_(nullptr) {}
  explicit ActionSequence(ActionNode* n) : i_(n) {}
  explicit ActionSequence(std::initializer_list<Action> a) : i_(nullptr) {
    for (auto it = a.begin(); it != a.end(); ++it) {
      push_back(*it);
    }
  }
  bool operator==(ActionSequence& b) const {
    return i_ == b.i_;
  }
  void push_back(const Action a) {
    ActionNode *n = i_->and_then(a);
    i_ = n;
  }
  void back() {
    i_ = i_->p;
  }
  bool has_predecessor() {
    return (!(i_ == nullptr));
  }
  const Action& action() const {
    return i_->action;
  }
private:
  ActionNode *i_;
};


/* Conventions:
 * We are player 0, clockwise (to our left) is player 1, to our right
 * is player 2. After a player folds, they don't take any more
 * actions (see the Do method).
 * Both the blind posts are handled as Bet actions.
 * The action sequence is a vector (TODO: reverse linked list?) of Actions, which can be
 * playeractions or card reveals. Board and phase can be calculated
 * from this list, or passed in.
 */

struct PublicInformationSet : public boost::static_visitor<void>{
  static const int num_players = 3;
  static const int big_blind = 2;
  PublicInformationSet(int n,
                 std::vector<int> ss,
                 int b)
    : hand_num(n),
      stack_sizes(ss),
      button_index(b) {
    assert(ss.size() == 3);
    turn_index = Clockwise(button_index, 1);
    for (int i = 0; i < num_players; ++i) {
      if (ss[i] == 0) {
        turn_index = button_index;
        has_folded[i] = true;
      }
    }
  }
  /* Is this action valid in the current state? */
  bool IsValid(Fold) const;
  bool IsValid(Check) const;
  bool IsValid(Call) const;
  bool IsValid(Bet) const;
  bool IsValid(Raise) const;
  bool IsValid(Post) const;
  bool IsValid(Card) const;
  //Applying this object to an action advances the gamestate by that action.
  void operator()(Fold);
  void operator()(Check);
  void operator()(Call);
  void operator()(Bet);
  void operator()(Raise);
  void operator()(Post);
  void operator()(Card);
  /* Run all refunds that must be made.
   * What a niche action. Eg if a player bets a lot, another player goes
   * all-in for a small amount, and the third player folds. The rest of
   * player one's bet is returned to her. */
  void Refund();
  /* Update heuristic values of the pockets. Used when we see a new
   * card or a player folds. */
  void UpdatePocketBuckets();
  void DoActions(std::vector<Action> a);
  void Do(Action a);

  /* Used for iteration: views a successor publicinfoset without
   * constructing a wholly new actions field. */
  const PublicInformationSet* ResultOf(Action a) const;
  /* Is the current round of betting done? Just checks if everyone is
   * either all-in or has posted the current_bet, unless everyone
   * called the big blind, in which case she can raise so the round is
   * not over. */
  bool BettingRoundDone() const;
  static int Clockwise(int index, int amt) {
    int r = (index + amt) % num_players;
    if (r < 0) { r += num_players; }
    return r;
  }
  void NextTurnIndex() {
    if(turn_index == button_index) {
      ++button_has_acted;
    }
    turn_index = Clockwise(turn_index, 1);
  }
  int big_blind_index() const {
    int bb_index = Clockwise(button_index,2);
    while (has_folded[bb_index]) {
      bb_index = Clockwise(bb_index, 2);
    }
    return bb_index;
  }
  int current_player_role() const {
    int role = 0;
    while (Clockwise(button_index, role) != turn_index) { ++role; }
    return role;
  }
  //What number hand is it in the match?
  int hand_num;
  std::vector<int> stack_sizes; /* all constructors should initialize
                                 * this. */
  std::vector<bool> has_folded = std::vector<bool>(num_players, false);
  /* convenience */
  int players_left() const;
  /* amount in pot this betting round */
  std::vector<int> betting_round_amount_in_pot = std::vector<int>(num_players, 0);
  /* amount in pot all previous betting rounds */
  std::vector<int> past_amount_in_pot = std::vector<int>(num_players, 0);
  int total_amount_in_pot(int player) const {
    return past_amount_in_pot[player] + betting_round_amount_in_pot[player];
  }
  int minimum_stack() const {
    int min = stack_sizes[0];
    for (int i = 1; i < num_players; ++i) {
      if (stack_sizes[i] < min) {
        min = stack_sizes[i];
      }
    }
    return min;
  }
  int maximum_other_stack() const {
    int max = 0;
    for (int i = 1; i < num_players; ++i) {
      if (stack_sizes[Clockwise(turn_index,i)] > max) {
        max = stack_sizes[Clockwise(turn_index,i)];
      }
    }
    return std::min(max, stack_sizes[turn_index]);
  }
  int button_index{0};
  ActionSequence actions{};
  GamePhase phase;
  Hand board;
  int turn_index{0};
  /* The (total) bet you must reach. So the player at turn_index, to call,
   * must pay current_bet - total_in_pot(turn_index). */
  int current_bet{0};
  /* This is the difference between current_bet and the actual amts we
   * find in Bet objects */
  int last_round_bet{0};
  int num_min_raises{0};
  int num_half_raises{0};
  /* The amount that the most recent player to raise current_bet
   * raised it by. Used for calculating minimum bet/raise. */
  int minimum_bet_delta{big_blind};
  /* Not precisely the minimum -raise-. It could be the minimum bet.
   * It's the minimum bet if betting isn't open yet. These are the
   * min/max for the next player to act, after all of the actions in
   * actions.*/
  int minimum_raise() const {
    return std::min({
        current_bet - last_round_bet + minimum_bet_delta,
        maximum_other_stack() - last_round_bet
          });
  }
  int fractional_pot_raise(int fraction) const {
    int call_amt = current_bet - last_round_bet;
    int already_bet = betting_round_amount_in_pot[turn_index];
    int pot_size_if_call = pot + call_amt - already_bet;
    return std::max(std::min((pot_size_if_call/fraction) + call_amt, stack_sizes[turn_index] - last_round_bet), minimum_raise());
  }
  int maximum_raise() const {
    return fractional_pot_raise(1);
  }
  // For calculating EV, the total we have to call to
  int call_amount() const { return std::min(current_bet, stack_sizes[turn_index]); }
  int call_amount_this_round() const { return call_amount() - last_round_bet; }
  int cost_to_call() const { return call_amount() - total_amount_in_pot(turn_index); }
  int cards_seen{0};
  int pot{0};
  int betting_round_pot{0};
  // Used to determine if a round of betting can end.
  int button_has_acted = 0;
  // Convenience: has the round of betting opened?
  bool betting_opened = false;

  abstract_game::pocket_bucket_by_pocket_t buckets_by_pocket{};
  std::array<int,3> total_summary_bet_amounts{};
  std::list<std::pair<Action,int> >  capped_actions_with_player{};
  std::list<AbstractAction>  capped_abstractified_actions{};

  AbstractAction StateTranslationStep(Action a) const;
  static int Tartanian5Bin(std::vector<int> bins, int x);
private:
  /* Helper method to adjust the state during a bet, post, or
   * raise. Puts money in the pot so that the total contribution of
   * the current player to the pot is amt. */
  void PutInPotTo(int amt);
  /* Helper method for helper method. */
  void BetToWithinRound(int amt);
  /* Pops capped lists to the summary bet fields */
  void PopToSummaryBets();
  /* Bin as in Tartanian5, returning the index of the bin */
};

/* probabilistically translates states */  
class StateTranslator : public boost::static_visitor<AbstractAction> {
public:
  StateTranslator(std::vector<int> bins) : bins_(bins) {}
  AbstractAction operator()(Fold) const {
    return Fold{};
  }
  AbstractAction operator()(Check) const {
    return Check{};
  }
  AbstractAction operator()(Call) const {
    return Call{0};
  }
  AbstractAction operator()(Bet o) const {
    return Bet{BinAmount(o.amt)};
  }
  AbstractAction operator()(Raise o) const {
    return Raise{BinAmount(o.amt)};
  }
  AbstractAction operator()(Card) const {
    return Card{0};
  }
  AbstractAction operator()(Post o) const {
    return o;
  }
private:
  std::vector<int> bins_;
  int BinAmount(int amt) const {
    int idx = PublicInformationSet::Tartanian5Bin(bins_,amt);
    return idx;
  }
};

/* translates abstract actions back to real actions; should have
 * remembered to use it in NextActionsPossible */  
class PlayerActionTranslator : public boost::static_visitor<PlayerAction> {
public:
  PlayerActionTranslator(const PublicInformationSet *p) : state_(p) {}
  PlayerAction operator()(Fold) const {
    return Fold{};
  }
  PlayerAction operator()(Check) const {
    return Check{};
  }
  PlayerAction operator()(Call) const {
    return Call{state_->call_amount_this_round()};
  }
  PlayerAction operator()(Bet o) const {
    return Bet{UnbinAmount(o.amt)};
  }
  PlayerAction operator()(Raise o) const {
    return Raise{UnbinAmount(o.amt)};
  }
private:
  const PublicInformationSet *state_;
  int UnbinAmount(int idx) const {
    assert(idx >= 0);
    assert(idx < 3);
    switch (idx) {
    case 0:
      return state_->minimum_raise();
    case 1:
      return state_->fractional_pot_raise(2);
    case 2:
      return state_->maximum_raise();
    default:
      assert(false);
    }
  }
};
/* translates abstract actions back to real actions */  
class ActionTranslator : public boost::static_visitor<Action> {
public:
  ActionTranslator(const PublicInformationSet *p) : state_(p) {}
  Action operator()(Fold) const {
    return Fold{};
  }
  Action operator()(Check) const {
    return Check{};
  }
  Action operator()(Call) const {
    return Call{state_->call_amount_this_round()};
  }
  Action operator()(Bet o) const {
    return Bet{UnbinAmount(o.amt)};
  }
  Action operator()(Raise o) const {
    return Raise{UnbinAmount(o.amt)};
  }
  Action operator()(Card) const {
    /* Should only happen during tree walking */
    return Card{0};
  }
  Action operator()(Post o) const {
    return o;
  }
private:
  const PublicInformationSet *state_;
  int UnbinAmount(int idx) const {
    assert(idx >= 0);
    assert(idx < 3);
    switch (idx) {
    case 0:
      return state_->minimum_raise();
    case 1:
      return state_->fractional_pot_raise(2);
    case 2:
      return state_->maximum_raise();
    default:
      assert(false);
    }
  }
};

#endif // PUBLIC_INFORMATION_SET_H_
