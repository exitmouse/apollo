#ifndef POCKET_BUCKET_H_
#define POCKET_BUCKET_H_
#include <array>
#include <forward_list>
#include "poker.h"

namespace abstract_game {
const static int MAX_HAND_BUCKETS = 4;

typedef uint8_t PocketBucket;
PocketBucket BucketPocket(Hand pocket, Hand board, int num_cards, int num_players, int max_buckets);
typedef std::array<PocketBucket, 1326> pocket_bucket_by_pocket_t;
pocket_bucket_by_pocket_t AllBuckets(Hand board, int num_cards, int num_players);
class PocketBucketStack {
public:
  PocketBucketStack() {
    push_frame(AllBuckets(Hand{0}, 0, 3));
  }
  void push_frame(pocket_bucket_by_pocket_t f) {
    stack_.push_front(f);
  }
  void pop() {
    stack_.pop_front();
  }
  PocketBucket get(int hand_index) {
    assert(!stack_.empty());
    return stack_.front()[hand_index];
  }
  std::forward_list<pocket_bucket_by_pocket_t> stack_;
};

} // namespace abstract_game

#endif // POCKET_BUCKET_H_
