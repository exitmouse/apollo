/* timer.h:
 * A namespace full of timing utilities. */
#include <chrono>
#include <utility> //std::forward

namespace timer {
  namespace sc = std::chrono;
  /* based on/stolen from
   * http://stackoverflow.com/questions/2808398/easily-measure-elapsed-time/21995693#21995693 */
  template<typename F, typename TimeT = sc::milliseconds, typename ...Args>
  TimeT Duration(F func, Args&&... args) {
    sc::system_clock::time_point start = sc::system_clock::now();
    func(std::forward<Args>(args)...);
    sc::system_clock::time_point end = sc::system_clock::now();
    return sc::duration_cast<TimeT>(end - start);
  }
}
