#ifndef STRATEGY_H_
#define STRATEGY_H_

#include "stdint.h"
#include <boost/functional/hash.hpp>
#include "bot/game_abstracter.h"
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include "game/action.h"
#include <boost/serialization/variant.hpp>
#include <unordered_map>
#include "unordered_map.hpp"

class ActionDistribution {
public:
  /* accesses regrets. These are not normalized. */
  const double& operator[](int8_t i) const {
    return dist_[i+1];
  }
  void SetIndex(int8_t i, double val) {
    assert( InBounds(i) );
    double oldval = dist_[i+1];
    dist_[i+1] = val;
    dist_[0] += val - oldval;
  }
  //Normalization factor
  double TotalLikelihood() const {
    return dist_[0];
  }
  /* accesses probabilities, which are normalized. */
  double ProbabilityOf(int8_t i) const {
    assert( InBounds(i) );
    if (dist_[0] > 0) {
      return dist_[i+1]/TotalLikelihood();
    } else {
      return -1;
    }
  }
  bool InBounds(int8_t i) const {
    assert(dist_.size() > 0);
    if ((i+1) > 0 && (i+1) < dist_.size()) { return true; }
    return false;
  }
  size_t size() const {
    return dist_.size() - 1;
  }
private:
  friend class boost::serialization::access;
  //keep track of total likelihood in first index for normalization
  /* vital to value initialize */
  std::array<double, 5 + 1> dist_{}; //5 = max abstract actions: fcmhp

  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & dist_;
  }
};

class Regret {
public:
  Regret() {}
  bool InBounds(abstract_game::AbstractInformationSet a) {
    if (map_.find(a) == map_.end()) {
      return false;
    } else {
      return true;
    }
  }
  ActionDistribution& operator[](abstract_game::AbstractInformationSet a) {
    try {
      return map_.at(a);
    } catch (std::out_of_range) {
      map_[a] = ActionDistribution{};
      return map_[a];
    }
  }

  std::tr1::unordered_map<abstract_game::AbstractInformationSet, ActionDistribution, abstract_game::aishash> map_{};

  template<class Archive>
  void serialize(Archive & ar, const unsigned int /* version */) {
    ar & map_;
  }

};

typedef Regret Strategy;

#endif // STRATEGY_H_
