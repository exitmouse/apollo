#include <gtest/gtest.h>
#include "game/public_information_set.h"
#include "bot/game_abstracter.h"

class PublicInformationSetTest : public ::testing::Test {
public:
  PublicInformationSet *info;
  PublicInformationSet *info_stacked; //cash money
  PublicInformationSet *info_poverty; //hard times
  PublicInformationSet *info_headsup; //down & out
  virtual void SetUp() {
    info = new PublicInformationSet(0,
                              std::vector<int>{55,3,200},
                              0);
    info_stacked = new PublicInformationSet(0,
                                      std::vector<int>{2000,2000,2000},
                                      0);
    info_poverty = new PublicInformationSet(0,
                                      std::vector<int>{10,10,1},
                                      0);
    info_headsup = new PublicInformationSet(0,
                                      std::vector<int>{498,98,0},
                                      0);
  }
  virtual void TearDown() {
    delete info;
  }
}; // class PublicInformationSetTest

TEST_F(PublicInformationSetTest, InitializesCorrectly) {
  info->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2},
            Call{2},
              Check{}, //big blind check
                Card{3,0},
                  Card{4,2},
                    Card{10,2},
                      Check{},
                        Bet{4},
                          Raise{18}, //max raise
                            Call{1} //all-in
    });
  EXPECT_EQ(info->current_bet, 20);
  EXPECT_EQ(info->cards_seen, 3);
  EXPECT_EQ(info->pot, 29);
}

TEST_F(PublicInformationSetTest, ValidatesBigBlindCheck) {
  info->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Fold{},
            Call{2}
    });
  EXPECT_TRUE(info->IsValid(Check{}));
  EXPECT_FALSE(info->IsValid(Call{2}));
}

TEST_F(PublicInformationSetTest, ValidatesBigBlindCallsOtherDirection) {
  info->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2},
            Fold{}
    });
  EXPECT_TRUE(info->IsValid(Check{}));
  EXPECT_FALSE(info->IsValid(Call{2}));
}

TEST_F(PublicInformationSetTest, ValidatesBigBlindRaises) {
  info->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2},
            Fold{}
    });
  EXPECT_TRUE(info->IsValid(Raise{6}));
}

TEST_F(PublicInformationSetTest, ValidatesSmallBlindRaises) {
  info_stacked->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Fold{}
    });
  EXPECT_TRUE(info_stacked->IsValid(Raise{4}));
}

TEST_F(PublicInformationSetTest, ValidatesBets) {
  info->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2},
            Fold{},
              Check{}, //big blind
                Card{10,3},
                  Card{11,3},
                    Card{2,1}
    });
  EXPECT_TRUE(info->IsValid(Bet{2}));
}

TEST_F(PublicInformationSetTest, AllowsBigBlindAllIn) {
  info_poverty->Do(Post{1});
  EXPECT_TRUE(info_poverty->IsValid(Post{1}));
}

TEST_F(PublicInformationSetTest, HandlesBigBlindAllIn) {
  info_poverty->DoActions(
    std::vector<Action>{
      Post{1},
        Post{1}
    });
  EXPECT_TRUE(info_poverty->IsValid(Raise{6}));
  EXPECT_FALSE(info_poverty->IsValid(Call{1}));
  EXPECT_TRUE(info_poverty->IsValid(Call{2}));
}

TEST_F(PublicInformationSetTest, HandlesAllInSmallBets) {
  PublicInformationSet *small = new PublicInformationSet(0, std::vector<int>{10, 3, 10}, 0);
  small->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2},
            Call{2},
              Check{},
                Card{0},
                  Card{1},
                    Card{2}
    });
  EXPECT_EQ(2, small->last_round_bet);
  EXPECT_EQ(1, small->turn_index);
  EXPECT_EQ(3, small->stack_sizes[1]);
  EXPECT_TRUE(small->IsValid(Bet{1}));
  EXPECT_FALSE(small->IsValid(Bet{2}));
}

TEST_F(PublicInformationSetTest, GivesBBChanceToRaiseHeadsup) {
  info_headsup->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2}
    });
  EXPECT_TRUE(info_headsup->IsValid(Raise{4}));
  EXPECT_TRUE(info_headsup->IsValid(Raise{6}));
  EXPECT_TRUE(info_headsup->IsValid(Check{}));
}

TEST_F(PublicInformationSetTest, MinAndMaxRaiseAmounts) {
  info_stacked->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2}
    });
  EXPECT_EQ(2, info_stacked->current_bet);
  EXPECT_EQ(0, info_stacked->last_round_bet);
  EXPECT_EQ(2, info_stacked->minimum_bet_delta);
  EXPECT_EQ(4, info_stacked->minimum_raise());
  EXPECT_EQ(5, info_stacked->pot);
  EXPECT_EQ(4, info_stacked->fractional_pot_raise(4));
  EXPECT_EQ(5, info_stacked->fractional_pot_raise(2));
  EXPECT_EQ(8, info_stacked->maximum_raise());
}

TEST_F(PublicInformationSetTest, AbstractifiesAmounts) {
  info_stacked->DoActions(
    std::vector<Action>{
      Post{1},
        Post{2},
          Call{2}
    });
  /* if this fails it might only fail some of the time, it's
   * probabilistic */
  for (int i = 0; i < 10; ++i) {
    EXPECT_EQ(0, ActionAmount(info_stacked->StateTranslationStep(Raise{4})));
    EXPECT_EQ(2, ActionAmount(info_stacked->StateTranslationStep(Raise{5})));
    EXPECT_EQ(3, ActionAmount(info_stacked->StateTranslationStep(Raise{8})));
  }
}

TEST_F(PublicInformationSetTest, Fuzz) {
  PublicInformationSet *fuzz = new PublicInformationSet(0,
                                            std::vector<int>{590, 10,0},
                                            0);
  fuzz->DoActions(
    std::vector<Action>{
      Post{1}, //p3
        Post{2}, //apollo
          Call{2},
            Check{}, //apollo (bb)
              Card{10,1},
                Card{11,1},
                  Card{3,2},
                    Check{}, //apollo
                      Bet{3}, //p3
                        Call{3},
                          Card{4,2},
                            Check{},
                              Bet{4},
                                Call{4},
                                  Card{7,3},
                                    Check{} //apollo
    });
  //Bet of apollo's stack size by p3
  //valid even though it is less than the bb = min bet
  EXPECT_TRUE(fuzz->IsValid(Bet{1}));
}

TEST_F(PublicInformationSetTest, SecondMaxStackSize) {
  PublicInformationSet *fuzz = new PublicInformationSet(0,
                                            std::vector<int>{3, 10, 200},
                                            0);
  fuzz->DoActions(
    std::vector<Action>{
      Post{1}, //p2
        Post{2},
          Call{2},
            Call{2},
              Check{}, //bb
                Card{10,1},
                  Card{11,1},
                    Card{3,2},
                      Check{} //p2
    });
  //Bet of p1's stack size by p3, even though p2 has excess chips
  EXPECT_FALSE(fuzz->IsValid(Bet{1}));
}

TEST_F(PublicInformationSetTest, CannotRaiseLessThanCurrentBet) {
  PublicInformationSet *fuzz = new PublicInformationSet(0,
                                            std::vector<int>{1, 10, 200},
                                            0);
  fuzz->DoActions(
    std::vector<Action>{
      Post{1}, //p2
        Post{2}
    });
  EXPECT_TRUE(fuzz->IsValid(Call{1}));
  EXPECT_FALSE(fuzz->IsValid(Raise{1}));
}

TEST_F(PublicInformationSetTest, CanAllInRaise) {
  PublicInformationSet *fuzz = new PublicInformationSet(0,
                                            std::vector<int>{3, 10, 200},
                                            0);
  fuzz->DoActions(
    std::vector<Action>{
      Post{1}, //p2
        Post{2}
    });
  EXPECT_TRUE(fuzz->IsValid(Raise{3}));
}

TEST_F(PublicInformationSetTest, CallAmountThisRoundIsRight) {
  PublicInformationSet *fuzz = new PublicInformationSet(0,
                                            std::vector<int>{1, 10, 200},
                                            0);
  fuzz->DoActions(
    std::vector<Action>{
      Post{1}, //p2
        Post{2}
    });
  EXPECT_EQ(1, fuzz->call_amount_this_round());
  fuzz->Do(Call{1});
  fuzz->Do(Raise{4});
  EXPECT_EQ(4, fuzz->call_amount_this_round());
}
