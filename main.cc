#include <iostream>
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include "bot/marshal.h"
#include "montecarlo/monte_bot.h"
#include "cfr_plus.h"
#include "game/value.h"

#include <random>
#include <ctime> // necessary to seed the RNG

#include <fstream>

// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/thread.hpp>

// include the CFRbot
#include "cfr_bot.h"

namespace opt = boost::program_options;
namespace {
  Strategy *avg_strat;
  Regret *regret_prof;
  Strategy *short_avg_strat;
  Regret *short_regret_prof;
}

void PrintUsage(char*);

void PrintUsage(char *name) {
  std::cout << "Usage: " << name << " --train [-t THREADS] [-i ITERATIONS]" << std::endl;
  std::cout << "     | " << name << " [-h HOST] [PORT=3000]" << std::endl;
}

int train_main(int threads, int iters);

int main(int argc, char *argv[])
{
  avg_strat = new Strategy{};
  regret_prof = new Regret{};
  short_avg_strat = new Strategy{};
  short_regret_prof = new Regret{};

  // WE MUST SEED RNG
  srand (static_cast <unsigned> (time(0)));

  int thread_count = 1;
  int iteration_count = 1;
  std::string host = "localhost";
  std::string inregrets = "regrets.txt";
  std::string instrat = "strategy.txt";
  std::string inshortregrets = "short_regrets.txt";
  std::string inshortstrat = "short_strategy.txt";
  int port = 3000;
  // Parse out command line options.
  opt::options_description desc("Options");
  desc.add_options()
    ("train", "Run training instead of the bot")
    ("threads,t", opt::value<int>(&thread_count), "Thread count for training")
    ("iterations,i", opt::value<int>(&iteration_count), "Iteration count for training")
    ("strategy", opt::value<std::string>(&instrat), "Filename for input average strategy")
    ("regrets", opt::value<std::string>(&inregrets), "Filename for input regrets")
    ("short-strategy", opt::value<std::string>(&inshortstrat), "Filename for input truncated average strategy")
    ("short-regrets", opt::value<std::string>(&inshortregrets), "Filename for input truncated regrets")
    ("host,h", opt::value<std::string>(&host), "HOST")
    ("port", opt::value<int>(&port), "PORT")
  ;
  opt::positional_options_description p;
  p.add("port", 1);
  opt::variables_map vm;
  try {
    opt::store(opt::command_line_parser(argc, argv).
               options(desc).positional(p).
               allow_unregistered().run(), vm);
    opt::notify(vm);
  } catch (const std::exception& e) {
    PrintUsage(argv[0]);
    std::cerr << e.what() << std::endl;
    return 1;
  }
  for (std::pair<std::string, Strategy*> file_loader : {std::make_pair<>(instrat, avg_strat), std::make_pair<>(inshortstrat, short_avg_strat)})
  {
    try {
      std::ifstream ifs(file_loader.first);
      boost::archive::text_iarchive arch(ifs);
      arch >> *(file_loader.second);
    } catch (boost::archive::archive_exception) {
      continue;
    }
  }
  for (std::pair<std::string, Regret*> file_loader : {std::make_pair<>(inregrets, regret_prof), std::make_pair<>(inshortregrets, short_regret_prof)})
  {
    try {
      std::ifstream ifs(file_loader.first);
      boost::archive::text_iarchive arch(ifs);
      arch >> *(file_loader.second);
    } catch (boost::archive::archive_exception) {
      continue;
    }
  }
  if (vm.count("train")) { /* run training */
    train_main(thread_count, iteration_count);
  } else { /* run the bot */
    if (!vm.count("port")) { /* they didn't specify a port,
                                we should print usage */
      PrintUsage(argv[0]);
      return 1;
    }
    // Connect to the engine.
    tcp::iostream stream;
    char port_buf[5];
    sprintf(port_buf, "%d", port);
    std::cout << "Connecting to " << host << ":" << port << "\n";
    stream.connect(host, port_buf);
    if (!stream) {
      std::cout << "Could not connect to " << host << ":" << port << "\n";
      return 1;
    }
    CFRBot *cfrb = new CFRBot(*regret_prof, *short_regret_prof);
    Marshal marshal(cfrb);
    marshal.run(stream);
  }

  return 0;
}

struct IterStruct {
  IterStruct(int i) : times(i) {
  }
  int times;
  cfr::CFRPlusTrainer t{avg_strat, regret_prof, short_avg_strat, short_regret_prof};
  void operator()() {
    for (int i = 0; i < times; ++i) {
      t.Iterate();
    }
  }
};

int train_main(int threads, int iters) {
  poker_value::InitEvalTables();
  cfr::Initialize();
  std::cout << "Initialized CFR Hand Tables" << std::endl;
  std::vector<boost::thread> t;
  for (int i = 0; i < threads; ++i) {
    t.emplace_back(IterStruct{iters});
  }
  std::cout << "Made trainers\n";
  for (int i = 0; i < threads; ++i) {
    t[i].join();
  }
  std::cout << "Performed iterates\n";
  std::ofstream ofs_s("strategy20.txt");
  boost::archive::text_oarchive oa_s(ofs_s);
  oa_s << (*avg_strat);
  std::ofstream ofs_r("regrets20.txt");
  boost::archive::text_oarchive oa_r(ofs_r);
  oa_r << (*regret_prof);
  std::ofstream short_ofs_s("short_strategy20.txt");
  boost::archive::text_oarchive short_oa_s(short_ofs_s);
  oa_s << (*short_avg_strat);
  std::ofstream short_ofs_r("short_regrets20.txt");
  boost::archive::text_oarchive short_oa_r(short_ofs_r);
  oa_r << (*short_regret_prof);
  std::cout << "Wrote file payloads\n";
  return 0;
}
