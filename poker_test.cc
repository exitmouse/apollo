#include <gtest/gtest.h>
#include <vector>
#include "game/poker.h"

class HandTest : public ::testing::Test {
public:
  virtual void SetUp() {}
  virtual void TearDown() {}
}; // class HandTest

TEST_F(HandTest, ExpandsSingleCardsUp) {
  Hand three_of_clubs(Card(1,0));
  Hand four_of_clubs(Card(2,0));
  EXPECT_EQ(three_of_clubs.Expand(three_of_clubs).cards(), four_of_clubs.cards());
  EXPECT_EQ(three_of_clubs.Expand(four_of_clubs).cards(), three_of_clubs.cards());
}

TEST_F(HandTest, Lowest) {
  Hand o{Card{0}, Card{1}};
  Hand p{Card{7}, Card{10}};
  EXPECT_EQ(o.Lowest(), 0);
  EXPECT_EQ(o.SecondLowest(), 1);
  EXPECT_EQ(p.Lowest(), 7);
  EXPECT_EQ(p.SecondLowest(), 10);
}

TEST_F(HandTest, TwoHands) {
  for (Hand h = Hand::First(2); h.NextHand(52); ) {
    EXPECT_NE(h.Lowest(), h.SecondLowest());
    EXPECT_NE(h.Lowest(), -1);
    EXPECT_NE(h.SecondLowest(), -1);
  }
}

TEST_F(HandTest, ThereAre1326Hands) {
  int counter = 0;
  bool first_hand = false;
  bool last_hand = false;
  for (Hand h = Hand::First(2);true;) {
    if (h.cards() == Hand::First(2).cards()) { first_hand = true; }
    if (h.cards() == Hand{Card{51}, Card{50}}.cards()) { last_hand = true; }
    ++counter;
    if (!h.NextHand(52)) { break; }
  }
  EXPECT_EQ(counter, 1326);
  EXPECT_TRUE(first_hand);
}
