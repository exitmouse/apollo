#ifndef CFR_BOT_H_
#define CFR_BOT_H_

#include "bot/bot_interface.h"
#include "strategy.h"

class CFRBot : public BotInterface {
public:
  virtual PlayerAction GetAction(const InformationSet&);
  virtual void InputKeys(const KVList) {}
  virtual KVList GetKeys(const InformationSet&) { return KVList{}; }
  inline CFRBot(Strategy strat, Strategy short_strat) : strategy(strat), short_strategy(short_strat) {}
private:
  PlayerAction ActionTranslate(const InformationSet&, AbstractPlayerAction);
  Strategy strategy;
  Strategy short_strategy;
};

#endif // CFR_BOT_H_
