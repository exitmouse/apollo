#include "../game/poker.h"

class MontecarloEvaluator{

public:
inline MontecarloEvaluator(Hand hero, Hand board, int cards_on_board, int pc) : hero_state_(hero), board_state_(board), cards_on_board_(cards_on_board), player_count_(pc) {
}
	
/* RunIteration
 * run a single iteration of Montecarlo, filling in the remainder of the table
 * state randomly (i.e. giving random cards to player who is not the hero
 * and filling out the rest of the board cards randomly as appropriate)
 * 
 * Returns: bool indicating whether hero won
 */
bool RunIteration() const;

/* RunIterationsByCount
 *
 * run count iterations and returns how often the hero won (0.0 - 1.0)
 *
 * Required: 
 *   [0] count: the number of iterations to run
 * 
 * Returns: frequency that hero won (0.0 - 1.0)
 */
double RunIterationsByCount(int count) const;

/* RunIterationsByTime
 *
 * Required: 
 *   [0] ms: the duration (in ms) to run Montecarlo for and return
 *   the frequency of victories
 * 
 * Returns: frequency that hero won (0.0 - 1.0)
 */
double RunIterationsByTime(long ms) const;

private:
Hand hero_state_;
Hand board_state_;
int cards_on_board_;
int player_count_;
};
