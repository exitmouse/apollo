#ifndef MONTE_BOT_H_
#define MONTE_BOT_H_

#include "../bot/bot_interface.h"

class MonteBot : public BotInterface {
public:
  virtual PlayerAction GetAction(const InformationSet&);
  virtual void InputKeys(const KVList) {}
  virtual KVList GetKeys(const InformationSet&) { return KVList{}; }
};

#endif // MONTE_BOT_H_
