#include "montecarlo.h"
#include "../game/poker.h"
#include "../game/value.h"
#include <ctime>

bool MontecarloEvaluator::RunIteration() const {
  // first, determine number of cards we'll need to grab for the board
  int cards_to_gen = 5 - cards_on_board_;
	
  uint64_t completed_board_state = board_state_.cards();
  uint64_t used_cards = hero_state_.cards() | completed_board_state;

  for(int i = 0; i < cards_to_gen; i++) {
    uint64_t new_card = Card::RandomCard(used_cards);
    completed_board_state |= new_card;
    used_cards |= new_card;
  }

  uint64_t hero_score = poker_value::Eval7(hero_state_.cards() | completed_board_state);

  // for each opponent, generate two random cards and see if they beat us
  for(int i = 0; i < player_count_ - 1; i++) {

    uint64_t first_card = Card::RandomCard(used_cards);
    used_cards |= first_card;

    uint64_t second_card = Card::RandomCard(used_cards);
    used_cards |= second_card; 

    uint64_t opp_score = poker_value::Eval7(completed_board_state | first_card | second_card);
    if(opp_score > hero_score) {
      return false;
    }
  }

  // if we've gotten through all of our opponents and we still have the highest score, we've won!
  return true;

}

double MontecarloEvaluator::RunIterationsByCount(int count) const {
  double wins = 0.0;
  for(int i = 0; i < count; i++) {
    if(MontecarloEvaluator::RunIteration()) {
      wins += 1.0;
    }
  }
  return wins / count;
}

double MontecarloEvaluator::RunIterationsByTime(long ms) const {
  std::clock_t start = std::clock();
  int times = 0;
  double running_tot = 0.0;
  while( ( ( std::clock() - start ) / static_cast<double>(CLOCKS_PER_SEC / 1000) ) < ms) {
    running_tot += MontecarloEvaluator::RunIterationsByCount(1000);
    times+=1;
  }
  //printf("ran %d times\n", times*1000);
  return running_tot / times;
}
