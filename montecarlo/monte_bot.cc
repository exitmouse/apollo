#include "monte_bot.h"
#include "montecarlo.h"

PlayerAction MonteBot::GetAction(const InformationSet& s) {
  const PublicInformationSet &state = s.public_state;
  MontecarloEvaluator eval = MontecarloEvaluator(s.hand, state.board, state.cards_seen, state.players_left());
  // run the montecarlo sim
  double win_rate = eval.RunIterationsByTime(3);
  if (state.IsValid(Check())) {
    return Check();
  } else {
    //TODO side pot EV
    double expected_value = (win_rate * state.pot)  - ((1.0 - win_rate) * state.cost_to_call());
    //std::cout << "Expected value : " << expected_value << "\n";
    //std::cout << "With WR : " << win_rate << "\n";
    if (expected_value >= 0) {
      return Call(state.cost_to_call());
    } else {
      return Fold();
    }
  }
}
