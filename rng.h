/* rng.h: extern declaration of the random number generator. */
#ifndef RNG_H_
#define RNG_H_
#include <random>

std::mt19937 &GetMersenneTwister();

#endif // RNG_H_
