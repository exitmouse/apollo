#include "cfr_plus.h"
#include "bot/game_abstracter.h"
#include "game/poker.h"
#include "game/value.h"
#include <algorithm>
#include <memory>

namespace cfr {

utility_from_pocket_t Product(prob_by_pocket_t a, utility_from_pocket_t b) {
  /* ok to default initialize since we set every index */
  utility_from_pocket_t result;
  for (int i = 0; i < a.size(); ++i) {
    result[i] = (a[i] * b[i]);
  }
  return result;
}

void Initialize() {
  HandIndexTablesInitialize();
}

/* Only called after the river. Initializes val_sort. */
void CFRPlusTrainer::InitValSort(const PublicInformationSet *p) {
  assert(!val_sort_init);
  val_sort = new std::array<std::pair<uint64_t, int>, 1326>{};
  std::array<uint64_t,1326> &ith = *(GetIndexToHandTable());
  for (int hand_index = 0; hand_index < 1326; ++hand_index) {
    Hand h{ith[hand_index]};
    if (h.cards() & p->board.cards()) {
      (*val_sort)[hand_index] = std::make_pair(-1, hand_index);
    } else {

      /* non-random access so caching should be good, the ridiculous
       * start-up time isn't an issue when we're training, and I have
       * NIH syndrome dammit */
      uint64_t value = poker_value::Eval7(h.cards() | p->board.cards());
      (*val_sort)[hand_index] = std::make_pair(value, hand_index);
    }
  }
  /* since val_sort has the value as the first component, sorting it
   * in lexicographical order will sort the values, letting us use the
   * second component to invert the sort */
  std::sort(val_sort->begin(), val_sort->end());
  val_sort_init = true;
}
void CFRPlusTrainer::PopValSort() {
  assert(val_sort_init);
  delete val_sort;
  val_sort_init = false;
}
std::array<std::pair<uint64_t, int>, 1326>* CFRPlusTrainer::GetValSort() {
  assert(val_sort_init);
  return val_sort;
}

void CFRPlusTrainer::Iterate() {
  std::cout << "Starting an iteration...\n";
  /* stack sizes control treedepth */
  int stack = abstract_game::STARTING_STACK_SIZE;
  PublicInformationSet p{0, std::vector<int>{stack,stack,stack}, 0};
  p.DoActions(std::vector<Action>{Post{1}, Post{2}});
  assert(p.IsValid(Fold{}));
  for (int i = 0; i < 3; ++i) {
    walk_count = 0;
    PublicChanceSampleWalk(&p, i);
  }
  return;
}

utility_from_pocket_t CFRPlusTrainer::PublicChanceSampleWalk(const PublicInformationSet *p, int player_num) {
  ++walk_count;
  if (depth <= 15) {
    for (int i = 0; i < 16-depth; ++i) {
      std::cout << "====";
    }
    std::cout << " DEPTH: " << depth << std::endl;
  }
  if ((walk_count % 50) == 0) {
    std::cout << "Player " << player_num << ". We've stepped on " << walk_count << " states. Pot is: " << p->pot << std::endl;
    std::cout << "Last round bet: " << p->last_round_bet / abstract_game::LAST_ROUND_BET_BIN_WIDTH << std::endl;
    abstract_game::AbstractBetHistory bet_history = abstract_game::CondensedBetHistory(*p);
    std::cout << "Betting History is: ";
    for (auto it = bet_history.begin(); it != bet_history.end(); ++it) {
      std::cout << StringAbstractAction(*it);
    }
    std::cout << std::endl;
  }
  /* button index is 0, we are player player_num */
  std::vector<AbstractPlayerAction> nexts = abstract_game::NextActionsPossible(*p);
  assert(nexts.size() <= 5); //fcmqhp
  // either it's time for a card, or we're terminal
  if (nexts.size() == 0) {
    if (p->IsValid(Card(47))) { // can we add a card?
      /* sample a random card */
      Card c = Card::RandomCardProper(p->board.cards());
      /* kill probability from pockets containing that card */
      probs_by_pocket_stack.push_card(c);
      /* copy p and advance the game state */
      const PublicInformationSet *copy = p->ResultOf(c);
      /* update pocket bucket data */
      buckets_by_pocket_stack.push_frame(copy->buckets_by_pocket);
      if (copy->phase == GamePhase::ON_RIVER) {
        InitValSort(copy);
      }
      /* recursively walk the tree */
      ++depth;
      utility_from_pocket_t u = PublicChanceSampleWalk(copy, player_num);
      /* pop the stack */
      --depth;
      buckets_by_pocket_stack.pop();
      if (copy->phase == GamePhase::ON_RIVER) {
        PopValSort();
      }
      delete copy;
      probs_by_pocket_stack.pop();
      return u;
    } else {
      /* terminal state, time to calculate utilities */
      utility_from_pocket_t ufp = CalculateUtilities(p, player_num);
      return ufp;
    }
  }
  utility_from_pocket_t total_ufp{};
  if (p->turn_index == player_num) {
    std::vector<utility_from_pocket_t> ufp_by_action{nexts.size()};
    for (int8_t i = 0; i < nexts.size(); ++i) {
      /* hack to reduce tree depth. Don't iterate over these parts of
         the tree, theoretically we should never assign probability
         to those actions as a result but the first iteration with
         uniform probability might be wonky. */
      if (nexts.size() == 5 && p->num_min_raises >= abstract_game::MAX_MIN_RAISES_IN_WALK && (i == 2)) {
        std::cout << "snicker-" << std::endl;
        continue;
      }
      if (nexts.size() == 5 && p->num_half_raises >= abstract_game::MAX_HALF_RAISES_IN_WALK && (i == 3)) {
        std::cout << "-----snack" << std::endl;
        continue;
      }
      if (nexts.size() == 4 && p->num_min_raises >= abstract_game::MAX_MIN_RAISES_IN_WALK && (i == 1)) {
        std::cout << "vorpal-" << std::endl;
        continue;
      }
      if (nexts.size() == 4 && p->num_half_raises >= abstract_game::MAX_HALF_RAISES_IN_WALK && (i == 2)) {
        std::cout << "-----blade??" << std::endl;
        continue;
      }
      /* not total probability, this is probability given p */
      prob_by_pocket_t sigma = RegretMatch(p, i, static_cast<int>(nexts.size()));
      /* push new probabilities of making it to this action */
      probs_by_pocket_stack.push_frame(sigma, player_num);
      /* copy p and advance the game state */
      const PublicInformationSet *copy = p->ResultOf(nexts[i]);
      buckets_by_pocket_stack.push_frame(copy->buckets_by_pocket);
      /* recursively walk the tree */
      ++depth;
      utility_from_pocket_t u_prime = PublicChanceSampleWalk(copy, player_num);
      /* pop the stack */
      --depth;
      buckets_by_pocket_stack.pop();
      delete copy;
      probs_by_pocket_stack.pop(player_num);
      /* ufps for pure strategy */
      ufp_by_action[i] = u_prime;
      /* current ufps */
      total_ufp = ComponentsAdd(total_ufp, Product(sigma, u_prime));
    }
    /* update the regrets and the average strategy */
    /* ------------------------------------------- */
    /* loop over hands, maintaining hand index */
    abstract_game::AbstractBetHistory bet_history = abstract_game::CondensedBetHistory(*p);
    abstract_game::AbstractBetHistory truncated_bet_history = abstract_game::TruncatedBetHistory(*p);
    abstract_game::AbstractInformationSet key{};
    key.bets = bet_history;
    key.phase = p->phase;
    key.current_player_role = p->current_player_role();
    key.last_round_bet = p->last_round_bet/abstract_game::LAST_ROUND_BET_BIN_WIDTH;
    /* button_index should be 0 here but this is how to do it
       elsewhere */
    assert(p->button_index == 0);
    key.has_folded = std::array<bool, 3>{p->has_folded[PublicInformationSet::Clockwise(p->button_index, 0)],
                                         p->has_folded[PublicInformationSet::Clockwise(p->button_index, 1)],
                                         p->has_folded[PublicInformationSet::Clockwise(p->button_index, 2)]};
    abstract_game::AbstractInformationSet short_key = key;
    short_key.bets = truncated_bet_history;
    short_key.last_round_bet = 0;
    for (int8_t a = 0; a < nexts.size(); ++a) {
      /* not total probability, this is probability given p */
      prob_by_pocket_t sigma = RegretMatch(p, a, static_cast<int>(nexts.size()));
      for (int hand_index = 0; hand_index < 1326; ++hand_index) {
        if (probs_by_pocket_stack.get(player_num, hand_index) < 0.0000001) { //TODO
          continue;
        } else {
          abstract_game::PocketBucket bucket = buckets_by_pocket_stack.get(hand_index);
          key.cards = bucket;
          short_key.cards = bucket;
          (*regret_profile)[key].SetIndex(a, std::max(0.0, (*regret_profile)[key][a] + ufp_by_action[a][hand_index] - total_ufp[hand_index]));
          (*average_strategy_profile)[key].SetIndex(a, (*average_strategy_profile)[key][a] + probs_by_pocket_stack.get(player_num, hand_index)*sigma[hand_index]);
          (*short_regret_profile)[short_key].SetIndex(a, std::max(0.0, (*short_regret_profile)[short_key][a] + ufp_by_action[a][hand_index] - total_ufp[hand_index]));
          (*short_average_strategy_profile)[short_key].SetIndex(a, (*short_average_strategy_profile)[short_key][a] + probs_by_pocket_stack.get(player_num, hand_index)*sigma[hand_index]);
        }
      }
    }
  } else {
    for (int8_t i = 0; i < nexts.size(); ++i) {
      /* hack to reduce tree depth. Don't iterate over these parts of
         the tree, theoretically we should never assign probability
         to those actions as a result but the first iteration with
         uniform probability might be wonky. */
      if (nexts.size() == 5 && p->num_min_raises >= abstract_game::MAX_MIN_RAISES_IN_WALK && (i == 2)) {
        std::cout << "snicker-" << std::endl;
        continue;
      }
      if (nexts.size() == 5 && p->num_half_raises >= abstract_game::MAX_HALF_RAISES_IN_WALK && (i == 3)) {
        std::cout << "-----snack" << std::endl;
        continue;
      }
      if (nexts.size() == 4 && p->num_min_raises >= abstract_game::MAX_MIN_RAISES_IN_WALK && (i == 1)) {
        std::cout << "vorpal-" << std::endl;
        continue;
      }
      if (nexts.size() == 4 && p->num_half_raises >= abstract_game::MAX_HALF_RAISES_IN_WALK && (i == 2)) {
        std::cout << "-----blade??" << std::endl;
        continue;
      }
      /* not our turn, so things are easy */
      /* not total probability, this is probability given p */
      prob_by_pocket_t sigma = RegretMatch(p, i, static_cast<int>(nexts.size()));
      /* push new probabilities of making it to this action */
      probs_by_pocket_stack.push_frame(sigma, player_num);
      /* copy p and advance the game state */
      const PublicInformationSet *copy = p->ResultOf(nexts[i]);
      /* recursively walk the tree */
      ++depth;
      utility_from_pocket_t u_prime = PublicChanceSampleWalk(copy, player_num);
      /* pop the stack */
      --depth;
      delete copy;
      probs_by_pocket_stack.pop(player_num);
      /* we have a pure strategy here: we always take no action, because
       * it's not our turn. so we just add u_prime to our total ufp for
       * each action. */
      total_ufp = ComponentsAdd(total_ufp, u_prime);
    }
  }
  return total_ufp;
}
utility_from_pocket_t CFRPlusTrainer::CalculateUtilities(const PublicInformationSet *p, int player_num) {
  std::array<int,3> amts{};
  for (int i = 0; i < 3; ++i) {
    amts[i] = p->total_amount_in_pot(i);
  }
  prob_by_pocket_t able_to_have_hand = PossibleHands(p->board.cards());
  utility_from_pocket_t win{};
  utility_from_pocket_t loss{};
  utility_from_pocket_t draw{};
  utility_from_pocket_t threeway{};
  //TODO here is where to tilt
  std::fill(win.begin(), win.end(), p->pot);
  std::fill(loss.begin(), loss.end(), -amts[player_num]);
  std::fill(draw.begin(), draw.end(), p->pot/2);
  std::fill(threeway.begin(), threeway.end(), p->pot/3);
  if (p->has_folded[player_num]) {
    return Product(able_to_have_hand, loss);
  } else if (p->players_left() == 1) {
    return Product(able_to_have_hand, win);
  } else {
    int opp1 = PublicInformationSet::Clockwise(player_num, 1);
    int opp2 = PublicInformationSet::Clockwise(player_num, 2);
    if (p->has_folded[opp1]) {
      std::array<prob_by_pocket_t,3> equities = PocketEquities(opp2);
      return Product(able_to_have_hand, ComponentsAdd(ComponentsAdd(Product(equities[0], win), Product(equities[1], loss)), Product(equities[2], draw)));
    } else if (p->has_folded[opp2]) {
      std::array<prob_by_pocket_t,3> equities = PocketEquities(opp1);
      return Product(able_to_have_hand, ComponentsAdd(ComponentsAdd(Product(equities[0], win), Product(equities[1], loss)), Product(equities[2], draw)));
    } else {
      std::array<prob_by_pocket_t,5> equities = PocketEquitiesVsTwoOpponents(opp1, opp2);
      return Product(able_to_have_hand,
                     ComponentsAdd(ComponentsAdd(ComponentsAdd(ComponentsAdd(Product(equities[0], win),
                                                                             Product(equities[1], loss)),
                                                               Product(equities[2], draw)),
                                                 Product(equities[3], draw)),
                                   Product(equities[4], threeway)));

    }
  }
}

/* win, loss, draw */
std::array<prob_by_pocket_t,3> CFRPlusTrainer::PocketEquities(int opp) {
  assert(val_sort_init);
  std::array<prob_by_pocket_t,3> equities{};
  //heads-up, and our opponent is idx i
  double prob_beneath = 0;
  std::array<double,52> prob_mass_beneath_with_card{};
  double prob_above = 0;
  std::array<double,52> prob_mass_above_with_card{};
  double prob_tied = 0;
  std::array<double,52> prob_mass_tied_with_card{};
  std::pair<std::array<int8_t, 1326>*, std::array<int8_t, 1326>*> itc = GetIndexToCardTables();
  std::array<int8_t,1326> &itc1 = *(itc.first);
  std::array<int8_t,1326> &itc2 = *(itc.second);
  /* loop over all hand indices first, so we know the total
   * probability */
  for (int i = 0; i < 1326; ++i) {
    prob_above += probs_by_pocket_stack.get(opp, i);
    /* add to prob mass containing first card of this hand */
    prob_mass_above_with_card[itc1[i]] += probs_by_pocket_stack.get(opp, i);
    /* add to prob mass containing second card of this hand */
    prob_mass_above_with_card[itc2[i]] += probs_by_pocket_stack.get(opp, i);
  }
  for (int value_index = 0; value_index < 1326; ++value_index) {
    uint64_t value = (*GetValSort())[value_index].first;
    int w = value_index + 1;
    for (; (w < 1326) && (*GetValSort())[w].first == value; ++w) {}
    /* in between value_index and w is a range of tied hands */
    for (int i = value_index; i < w; ++i) {
      int i_hand_index = (*GetValSort())[i].second;
      assert(i_hand_index < 1326);
      double prob_of_hand = probs_by_pocket_stack.get(opp, i_hand_index);
      prob_tied += prob_of_hand;
      prob_mass_tied_with_card[itc1[i_hand_index]] += prob_of_hand;
      prob_mass_tied_with_card[itc2[i_hand_index]] += prob_of_hand;
      prob_above -= prob_of_hand;
      prob_mass_above_with_card[itc1[i_hand_index]] -= prob_of_hand;
      prob_mass_above_with_card[itc2[i_hand_index]] -= prob_of_hand;
    }
    for (int i = value_index; i < w; ++i) {
      int i_hand_index = (*GetValSort())[i].second;
      assert(i_hand_index < 1326);
      double prob_of_hand = probs_by_pocket_stack.get(opp, i_hand_index);
      /* inclusion/exclusion */
      /* ------------------- */
      /* probability of winning */
      equities[0][i_hand_index] = prob_beneath;
      equities[0][i_hand_index] -= prob_mass_beneath_with_card[itc1[i_hand_index]];
      equities[0][i_hand_index] -= prob_mass_beneath_with_card[itc2[i_hand_index]];
      equities[0][i_hand_index] += prob_of_hand;
      /* probability of loss */
      equities[1][i_hand_index] = prob_above;
      equities[1][i_hand_index] -= prob_mass_above_with_card[itc1[i_hand_index]];
      equities[1][i_hand_index] -= prob_mass_above_with_card[itc2[i_hand_index]];
      equities[1][i_hand_index] += prob_of_hand;
      /* probability of tie */
      equities[2][i_hand_index] = prob_tied;
      equities[2][i_hand_index] -= prob_mass_tied_with_card[itc1[i_hand_index]];
      equities[2][i_hand_index] -= prob_mass_tied_with_card[itc2[i_hand_index]];
      equities[2][i_hand_index] += prob_of_hand;
    }
    for (int8_t c = 0; c < 52; ++c) {
      prob_mass_beneath_with_card[c] += prob_mass_tied_with_card[c];
      prob_mass_tied_with_card[c] = 0;
    }
    prob_beneath += prob_tied;
    prob_tied = 0;
    value_index = w;
  }
  return equities;
}

/* win, loss, tied with opp1, tied with opp2, 3way tie.
 * O(n^2) instead of O(n^3), but linear time may be possible. */
std::array<prob_by_pocket_t,5> CFRPlusTrainer::PocketEquitiesVsTwoOpponents(int opp1, int opp2) {
  assert(val_sort_init);
  std::array<prob_by_pocket_t,5> equities{};
  /* prob_mass_x_with_card[c] is the contribution to prob_x from
   * situations where exactly one opponent has card c */
  double prob_beneath = 0;
  std::array<double,52> prob_mass_beneath_with_card{};
  std::array<double,1326> prob_mass_beneath_by_hand_index{};
  double prob_above = 0;
  std::array<double,52> prob_mass_above_with_card{};
  std::array<double,1326> prob_mass_above_by_hand_index{};
  double prob_tied1 = 0;
  std::array<double,52> prob_mass_tied1_with_card{};
  std::array<double,1326> prob_mass_tied1_by_hand_index{};
  double prob_tied2 = 0;
  std::array<double,52> prob_mass_tied2_with_card{};
  std::array<double,1326> prob_mass_tied2_by_hand_index{};
  double prob_tied_both = 0;
  std::array<double,52> prob_mass_tied_both_with_card{};
  std::array<double,1326> prob_mass_tied_both_by_hand_index{};
  std::pair<std::array<int8_t, 1326>*, std::array<int8_t, 1326>*> itc = GetIndexToCardTables();
  std::array<int8_t,1326> &itc1 = *(itc.first);
  std::array<int8_t,1326> &itc2 = *(itc.second);
  std::array<uint64_t,1326> &ith = *(GetIndexToHandTable());
  /* loop over all hand indices first, so we know the total
   * probability */
  for (int i = 0; i < 1326; ++i) {
    for (int j = 0; j < 1326; ++j) {
      if(ith[i] & ith[j]) { continue; }
      double prob_i = probs_by_pocket_stack.get(opp1, i);
      double prob_j = probs_by_pocket_stack.get(opp2, j);
      prob_above += prob_i*prob_j;
      /* add to prob mass containing first card of this hand */
      prob_mass_above_with_card[itc1[i]] += prob_i*prob_j;
      /* add to prob mass containing second card of this hand */
      prob_mass_above_with_card[itc2[i]] += prob_i*prob_j;
      /* add to prob mass for the 4 choose 2 hands contained
       * in the union of our opponents hands */
      /* hand indices for those hands */
      assert(ith[j]);
      std::array<int, 6> hands = GetHandsIn(i,j);
      for (auto it = hands.begin(); it != hands.end(); ++it) {
        prob_mass_above_by_hand_index[(*it)] += prob_i*prob_j;
      }
    }
  }
  for (int value_index = 0; value_index < 1326; ++value_index) {
    uint64_t value = (*GetValSort())[value_index].first;
    int w = value_index + 1;
    /* in between value_index and w is a range of tied hands */
    for (; (w < 1326) && (*GetValSort())[w].first == value; ++w) {}
    for (int i = value_index; i < w; ++i) {
      int i_hand_index = (*GetValSort())[i].second;
      /* only probability masses that can change are the ones which
       * correspond to either opp1 having one of these tied hands,
       * opp2 having one of these tied hands, or both.
       * Where other_opp has a hand that beats i, increasing the value of
       * our hand into i won't change any of our equities. So we loop
       * over other_opp hand values which are less than w. This also
       * restricts to other_opp_val < opp_val, so we don't have to
       * halve prob_above or anything*/
      for (int other_opp_value_index = 0; other_opp_value_index < w; ++other_opp_value_index) {
        int other_opp_hand_index = (*GetValSort())[other_opp_value_index].second;
        if (ith[i_hand_index] & ith[other_opp_hand_index]) { continue; }
        assert(ith[other_opp_hand_index]);
        std::array<int, 6> hands = GetHandsIn(i_hand_index,other_opp_hand_index);
        if (other_opp_value_index < i) {
          /* part where other_opp has a lower hand. */
          /* opp = opp1, other_opp = opp2 */
          assert(i_hand_index < 1326);
          assert(other_opp_hand_index < 1326);
          double prob_of_hands = probs_by_pocket_stack.get(opp1, i_hand_index) * probs_by_pocket_stack.get(opp2, other_opp_hand_index);
          prob_tied1 += prob_of_hands;
          prob_mass_tied1_with_card[itc1[i_hand_index]] += prob_of_hands;
          prob_mass_tied1_with_card[itc2[i_hand_index]] += prob_of_hands;
          prob_mass_tied1_with_card[itc1[other_opp_hand_index]] += prob_of_hands;
          prob_mass_tied1_with_card[itc2[other_opp_hand_index]] += prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_tied1_by_hand_index[(*it)] += prob_of_hands;
          }
          prob_above -= prob_of_hands;
          prob_mass_above_with_card[itc1[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc1[other_opp_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[other_opp_hand_index]] -= prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_above_by_hand_index[(*it)] -= prob_of_hands;
          }
          /* opp = opp2, other_opp = opp1 */
          prob_of_hands = probs_by_pocket_stack.get(opp2, i_hand_index) * probs_by_pocket_stack.get(opp1, other_opp_hand_index);
          prob_tied2 += prob_of_hands;
          prob_mass_tied2_with_card[itc1[i_hand_index]] += prob_of_hands;
          prob_mass_tied2_with_card[itc2[i_hand_index]] += prob_of_hands;
          prob_mass_tied2_with_card[itc1[other_opp_hand_index]] += prob_of_hands;
          prob_mass_tied2_with_card[itc2[other_opp_hand_index]] += prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_tied2_by_hand_index[(*it)] += prob_of_hands;
          }
          prob_above -= prob_of_hands;
          prob_mass_above_with_card[itc1[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc1[other_opp_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[other_opp_hand_index]] -= prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_above_by_hand_index[(*it)] -= prob_of_hands;
          }
        } else /* other_opp_value_index < w */ {
          /* part where other_opp has a tied hand as well, doesn't
           * matter which is opp1 and which is opp2 */
          double prob_of_hands = probs_by_pocket_stack.get(opp1, i_hand_index) * probs_by_pocket_stack.get(opp2, other_opp_hand_index);
          prob_tied_both += prob_of_hands;
          prob_mass_tied_both_with_card[itc1[i_hand_index]] += prob_of_hands;
          prob_mass_tied_both_with_card[itc2[i_hand_index]] += prob_of_hands;
          prob_mass_tied_both_with_card[itc1[other_opp_hand_index]] += prob_of_hands;
          prob_mass_tied_both_with_card[itc2[other_opp_hand_index]] += prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_tied_both_by_hand_index[(*it)] += prob_of_hands;
          }
          prob_above -= prob_of_hands;
          prob_mass_above_with_card[itc1[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[i_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc1[other_opp_hand_index]] -= prob_of_hands;
          prob_mass_above_with_card[itc2[other_opp_hand_index]] -= prob_of_hands;
          for (auto it = hands.begin(); it != hands.end(); ++it) {
            prob_mass_above_by_hand_index[(*it)] -= prob_of_hands;
          }
        }
      }
    }
    for (int i = value_index; i < w; ++i) {
      int i_hand_index = (*GetValSort())[i].second;
      /* inclusion/exclusion */
      /* ------------------- */
      /* probability of winning */
      equities[0][i_hand_index] = prob_beneath;
      equities[0][i_hand_index] -= prob_mass_beneath_with_card[itc1[i_hand_index]];
      equities[0][i_hand_index] -= prob_mass_beneath_with_card[itc2[i_hand_index]];
      /* the probability two random different hands are beneath us,
       * minus the probability that two random different hands beneath
       * us contain the first card, minus the probability that two
       * random different hands beneath us contain the second card, so
       * we need to add back in the probability that two random
       * different hands beneath us, together, contain both of our
       * cards. This is stored in
       * prob_mass_beneath_by_hand_index[i_hand_index], and already
       * separately includes the probability opp1 has our hand and
       * the probability opp2 has our hand. All of the other 5 are
       * the same. */
      equities[0][i_hand_index] += prob_mass_beneath_by_hand_index[i_hand_index];
      /* probability of loss */
      equities[1][i_hand_index] = prob_above;
      equities[1][i_hand_index] -= prob_mass_above_with_card[itc1[i_hand_index]];
      equities[1][i_hand_index] -= prob_mass_above_with_card[itc2[i_hand_index]];
      equities[1][i_hand_index] += prob_mass_above_by_hand_index[i_hand_index];
      /* probability of tie1 */
      equities[2][i_hand_index] = prob_tied1;
      equities[2][i_hand_index] -= prob_mass_tied1_with_card[itc1[i_hand_index]];
      equities[2][i_hand_index] -= prob_mass_tied1_with_card[itc2[i_hand_index]];
      equities[2][i_hand_index] += prob_mass_tied1_by_hand_index[i_hand_index];
      /* probability of tie2 */
      equities[3][i_hand_index] = prob_tied2;
      equities[3][i_hand_index] -= prob_mass_tied2_with_card[itc1[i_hand_index]];
      equities[3][i_hand_index] -= prob_mass_tied2_with_card[itc2[i_hand_index]];
      equities[3][i_hand_index] += prob_mass_tied2_by_hand_index[i_hand_index];
      /* probability of tied_both */
      equities[4][i_hand_index] = prob_tied_both;
      equities[4][i_hand_index] -= prob_mass_tied_both_with_card[itc1[i_hand_index]];
      equities[4][i_hand_index] -= prob_mass_tied_both_with_card[itc2[i_hand_index]];
      equities[4][i_hand_index] += prob_mass_tied_both_by_hand_index[i_hand_index];
    }
    for (int i = 0; i < 1326; ++i) {
      prob_mass_beneath_by_hand_index[i] += prob_mass_tied1_by_hand_index[i];
      prob_mass_beneath_by_hand_index[i] += prob_mass_tied2_by_hand_index[i];
      prob_mass_beneath_by_hand_index[i] += prob_mass_tied_both_by_hand_index[i];
      prob_mass_tied1_by_hand_index[i] = 0;
      prob_mass_tied2_by_hand_index[i] = 0;
      prob_mass_tied_both_by_hand_index[i] = 0;
    }
    for (int8_t c = 0; c < 52; ++c) {
      prob_mass_beneath_with_card[c] += prob_mass_tied1_with_card[c];
      prob_mass_beneath_with_card[c] += prob_mass_tied2_with_card[c];
      prob_mass_beneath_with_card[c] += prob_mass_tied_both_with_card[c];
      prob_mass_tied1_with_card[c] = 0;
      prob_mass_tied2_with_card[c] = 0;
      prob_mass_tied_both_with_card[c] = 0;
    }
    prob_beneath += prob_tied1;
    prob_beneath += prob_tied2;
    prob_beneath += prob_tied_both;
    prob_tied1 = 0;
    prob_tied2 = 0;
    prob_tied_both = 0;
    value_index = w;
  }
  return equities;
}

/* TODO normalize?? should not actually matter */
prob_by_pocket_t PossibleHands(uint64_t cards) {
  prob_by_pocket_t ret;
  std::array<uint64_t,1326> &ith = *(GetIndexToHandTable());
  for (int hand_index = 0; hand_index < 1326; ++hand_index) {
    Hand h{ith[hand_index]};
    if (h.cards() & cards) {
      ret[hand_index] = 0;
    } else {
      ret[hand_index] = 1;
    }
  }
  return ret;
}

prob_by_pocket_t CFRPlusTrainer::RegretMatch(const PublicInformationSet *p, int8_t a, int num_choices) {
  assert(p);
  assert(a < 5); //fcmhp
  abstract_game::AbstractBetHistory bet_history = abstract_game::CondensedBetHistory(*p);
  abstract_game::AbstractInformationSet key{};
  key.phase = p->phase;
  key.current_player_role = p->current_player_role();
  key.last_round_bet = p->last_round_bet/abstract_game::LAST_ROUND_BET_BIN_WIDTH;
  key.has_folded = std::array<bool, 3>{p->has_folded[PublicInformationSet::Clockwise(p->button_index, 0)],
                                       p->has_folded[PublicInformationSet::Clockwise(p->button_index, 1)],
                                       p->has_folded[PublicInformationSet::Clockwise(p->button_index, 2)]};
  key.bets = bet_history;
  prob_by_pocket_t probs{};
  for (int hand_index = 0; hand_index < 1326; ++hand_index) {
    abstract_game::PocketBucket bucket = buckets_by_pocket_stack.get(hand_index);
    key.cards = bucket;
    double prob = (*regret_profile)[key].ProbabilityOf(a);
    //if that's not been initialized yet we are uniform
    if (prob < 0.0001) { probs[hand_index] = 1/static_cast<double>(num_choices); }
    else { probs[hand_index] = prob; }
  }
  return probs;
}


} // namespace cfr
